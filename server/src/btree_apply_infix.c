/*
** btree_apply_infix.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 10:00:47 2012 julien lebot
** Last update Wed Apr 24 17:40:26 2013 julien lebot
*/

#include "btree.h"

void	btree_apply_infix(t_btree *root,
			  t_btree_apply_fn applyf,
			  void *ctx)
{
  if (root->left)
    btree_apply_infix(root->left, applyf, ctx);
  applyf(root->data, ctx);
  if (root->right)
    btree_apply_infix(root->right, applyf, ctx);
}
