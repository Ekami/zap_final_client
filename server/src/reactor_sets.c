/*
** reactor_sets.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Fri Apr 26 23:33:39 2013 julien lebot
** Last update Sun Apr 28 01:48:17 2013 julien lebot
*/

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "btree.h"
#include "connect.h"
#include "reactor.h"

void		add_item_to_set(t_connection *item, fd_set *sets)
{
  FD_SET(item->fd, &sets[e_read_set]);
  FD_SET(item->fd, &sets[e_write_set]);
}

int		configure_sets(t_btree *work_tree,
			       fd_set *sets)
{
  t_btree	*node;
  int		max_fd;

  FD_ZERO(&sets[e_read_set]);
  FD_ZERO(&sets[e_write_set]);
  max_fd = -1;
  btree_apply_infix(work_tree, (t_btree_apply_fn)&add_item_to_set, sets);
  node = work_tree;
  while (node && node->left)
    node = node->left;
  if (node)
    max_fd = ((t_connection *)node->data)->fd;
  return (max_fd);
}
