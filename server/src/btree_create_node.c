/*
** btree_create_node.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 09:19:03 2012 julien lebot
** Last update Sun Apr 28 14:54:37 2013 julien lebot
*/

#include <stdlib.h>
#include "btree.h"

t_btree		*btree_create_node(void *data)
{
  t_btree	*bt;

  if ((bt = malloc(sizeof(t_btree))) == NULL)
    return (NULL);
  bt->data = data;
  bt->left = NULL;
  bt->right = NULL;
  return (bt);
}

void		transfer_nodes(t_btree **root,
			       int (*cmpf)(void *, void *),
			       void (*freefn)(void *))
{
  t_btree	*old_node;

  old_node = *root;
  if ((*root)->left)
    {
      *root = (*root)->left;
      if (old_node->right)
	btree_insert_branch(*root, old_node->right, cmpf);
     }
  else if ((*root)->right)
    {
      *root = (*root)->right;
      if (old_node->left)
	btree_insert_branch(*root, old_node->left, cmpf);
    }
  else
    *root = NULL;
  if (freefn)
    freefn(old_node->data);
  free(old_node);
}

void		btree_free(t_btree *root, void (*freefn)(void *))
{
  if (!root)
    return;
  if (root->left)
    btree_free(root->left, freefn);
  if (root->right)
    btree_free(root->right, freefn);
  if (freefn)
    freefn(root->data);
  free(root);
}

void		btree_remove_item(t_btree **root,
				  void *data_ref,
				  int (*cmpf)(void *, void *),
				  void (*freefn)(void *))
{

  int	sign;

  sign = cmpf((*root)->data, data_ref);
  if (sign > 0 && (*root)->left)
    return (btree_remove_item(&(*root)->left, data_ref, cmpf, freefn));
  else if (sign < 0 && (*root)->right)
    return (btree_remove_item(&(*root)->right, data_ref, cmpf, freefn));
  else if (sign == 0)
    transfer_nodes(root, cmpf, freefn);
}
