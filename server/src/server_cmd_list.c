/*
** server_cmd_list.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 04:39:09 2013 julien lebot
** Last update Sun Apr 28 07:19:28 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server_commands.h"
#include "send_helpers.h"
#include "channel.h"

static void		do_server_list(t_btree *chan_node,
				       char *chan_name,
				       t_client *client,
				       t_server_data *data)
{
  t_str_key_value	*kvp;

  if (chan_node->left)
    do_server_list(chan_node->left, chan_name, client, data);
  if (chan_node->data)
    {
      kvp = (t_str_key_value *)chan_node->data;
      if (chan_name)
	{
	  if (strstr(kvp->key, chan_name) != NULL)
	    send_message((char *)kvp->key, 322, client, data);
	}
      else
	send_message((char *)kvp->key, 322, client, data);
    }
  if (chan_node->right)
    do_server_list(chan_node->right, chan_name, client, data);
}

void			server_cmd_list(int argc,
					char **argv,
					t_client *client,
					t_server_data *data)
{
  (void)argv;
  if (!client->nickname)
    return;
  if (data->channels)
    {
      if (argc == 1)
	do_server_list((t_btree *)data->channels, argv[0], client, data);
      else
	do_server_list((t_btree *)data->channels, NULL, client, data);
    }
  send_message("End of /LIST", 323, client, data);
}
