/*
** btree_search_item.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 10:29:05 2012 julien lebot
** Last update Tue Apr 23 13:55:44 2013 julien lebot
*/

#include "btree.h"

void	*btree_search_item(t_btree *root,
			   void *data_ref,
			   int (*cmpf)(void *, void *))
{
  int	sign;

  sign = cmpf(root->data, data_ref);
  if (!sign)
    return (root->data);
  else if (sign > 0 && root->left)
    return (btree_search_item(root->left, data_ref, cmpf));
  else if (root->right)
    return (btree_search_item(root->right, data_ref, cmpf));
  else
    return (0);
}
