/*
** my_str_find_last.c for  in /home/lebot_j//afs/myls
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Nov  1 23:19:34 2012 julien lebot
** Last update Fri Apr 26 00:11:36 2013 julien lebot
*/

#include <string.h>

int		my_str_find_last(const char *str, char c, int start)
{
  int		sz_str;

  sz_str = strlen(str);
  if (start > sz_str)
    return (-1);
  while (sz_str && *(str + sz_str - start) != c)
    sz_str--;
  return (sz_str - 1);
}

const char	*my_str_find_last_of(const char *str, char c)
{
  int	sz_str;

  sz_str = strlen(str);
  while (sz_str && *(str + sz_str) != c)
    sz_str--;
  return (str + sz_str - 1);
}
