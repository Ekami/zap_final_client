/*
** server_cmd_send_msg_to_user.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:46:37 2013 julien lebot
** Last update Sun Apr 28 04:41:23 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server_commands.h"
#include "send_helpers.h"

static void		do_send_message_to_user(t_client *client,
						t_message *msg)
{
  if (strcmp(client->nickname, msg->recipient) == 0)
    cb_write(&client->connect.write_buffer,
	     msg->buf->data,
	     msg->buf->idx);
}

void		send_message_to_user(t_buffer *message,
				     char *user,
				     t_client *client,
				     t_server_data *data)
{
  t_message	msg;

  (void)client;
  msg.buf = message;
  msg.recipient = user;
  btree_apply_infix(data->work_tree,
		    (t_btree_apply_fn)&do_send_message_to_user,
		    &msg);
}
