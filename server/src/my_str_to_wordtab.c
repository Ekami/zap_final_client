/*
** my_str_to_wordtab.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 05:02:04 2013 julien lebot
** Last update Sun Apr 28 05:02:05 2013 julien lebot
*/

#include <stdlib.h>
#include <string.h>

int		count_word(char *str)
{
  int		i;
  int		a;

  i = 0;
  a = 0;
  while (str[i] != '\0')
    {
      if (str[i] == ':')
	return (a + 1);
      while (str[i] == ' ' || str[i] == '\t')
	i++;
      if (str[i] == '\0')
	return (a);
      else
	a++;
      while (str[i] != '\0' && str[i] != ' ' && str[i] != '\t')
	i++;
      if (str[i] == '\0')
	return (a);
      i++;
    }
  return (a);
}

void		free_wordtab(char **str)
{
  int		i;

  i = 0;
  while (str[i])
    free(str[i++]);
  free(str);
}

static int	store_string(char *str, char **tab, int *j)
{
  int		len;
  int		i;

  i = *j;
  len = 0;
  while (str[*j] != '\0' && (str[*j] == ' ' || str[*j] == '\t'))
    (*j)++;
  i = *j;
  while (str[i] != '\0' && str[i] != ' ' && str[i] != '\t')
    {
      len++;
      i++;
    }
  if ((*tab = malloc(sizeof(char) * (len + 1))) == NULL)
    return (-1);
  i = 0;
  while (str[*j] != '\0' && str[*j] != ' ' && str[*j] != '\t')
    (*tab)[i++] = str[(*j)++];
  (*tab)[len] = '\0';
  return (0);
}

int		my_str_to_wordtab(char *str, char ***tab)
{
  int		i;
  int		j;
  int		count;

  j = 0;
  i = 0;
  count = count_word(str);
  if ((*tab = malloc(sizeof(char*) * (count + 1))) == NULL)
    return (0);
  while (i != count)
    {
      if (store_string(str, &(*tab)[i], &j) < 0)
	return (i - 1);
      i++;
    }
  (*tab)[i] = NULL;
  return (count);
}
