/*
** server_cmd_quit.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:44:58 2013 julien lebot
** Last update Sun Apr 28 04:59:46 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server_commands.h"
#include "send_helpers.h"
#include "channel.h"

void		server_cmd_quit(int argc,
				char **argv,
				t_client *c,
				t_server_data *data)
{
  t_double_list	*chans;
  char		buffer[BUFSIZE];
  t_buffer	l_buf;

  l_buf.cap = BUFSIZE;
  l_buf.data = buffer;
  l_buf.idx = write_user_response(l_buf.data, l_buf.cap, c);
  if (argc == 1)
    l_buf.idx += snprintf(l_buf.data + l_buf.idx, l_buf.cap - l_buf.idx,
		     "QUIT :Client Quit (%s)\r\n", argv[0]);
  else
    l_buf.idx += snprintf(l_buf.data + l_buf.idx, l_buf.cap - l_buf.idx,
		     "QUIT :Client Quit\r\n");
  chans = c->channels;
  while (chans)
    {
      send_message_to_channel(&l_buf,
			      ((t_channel *)chans->data)->name,
			      c, data);
      chans = chans->next;
    }
  c->connect.closing = 1;
}
