/*
** server_cmd_join.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 18:17:22 2013 julien lebot
** Last update Sun Apr 28 21:24:36 2013 julien lebot
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "server_commands.h"
#include "send_helpers.h"
#include "channel.h"
#include "string_utils.h"

static void		write_user_list_reply_header(t_buffer *buffer,
						     t_client *client,
						     t_server_data *data,
						     char *chan)
{
  int			written;

  buffer->idx = 0;
  memset(buffer->data, 0, buffer->cap);
  written = snprintf(buffer->data,
		     buffer->cap,
		     ":%s 353 %s = %s :",
		     data->server_name,
		     client->nickname,
		     chan);
  buffer->idx += written;
}

/*
** !BUG: if the number of clients is too damn high
** then there is a risk that the output ring buffer
** wraps around and overwrite part of the client
** list we sent ! There is no current workaround
** that.
*/
static void		send_user_list(t_btree *users,
				       t_client *client,
				       t_server_data *data,
				       t_buffer *buffer)
{
  t_str_key_value	*kvp;

  if (users->left)
    send_user_list(users->left, client, data, buffer);
  if (users->data)
    {
      kvp = (t_str_key_value *)users->data;
      if (strlen(kvp->key) >= (size_t)buffer->cap)
	{
	  snprintf(buffer->data + buffer->idx, buffer->cap + 2, "\r\n");
	  cb_write(&client->connect.write_buffer, buffer->data, buffer->idx);
	  buffer->idx = (char *)my_str_find_first_of(buffer->data, ':')
	    - buffer->data + 1;
	  memset(buffer->data + buffer->idx, 0, buffer->cap - buffer->idx);
	}
      buffer->idx += snprintf(buffer->data + buffer->idx,
			      buffer->cap - buffer->idx, " %s", kvp->key);
    }
  if (users->right)
    send_user_list(users->right, client, data, buffer);
}

static void		do_join_channel(t_channel *channel,
					t_client *client,
					t_server_data *data,
					t_buffer *buffer)
{
  char			buf[BUFSIZE];
  t_buffer		l_buf;

  l_buf.data = buf;
  l_buf.cap = BUFSIZE - 2;
  l_buf.idx = 0;
  strmap_insert(&channel->users, client->nickname, client);
  dl_insert(&client->channels, channel);
  l_buf.idx = write_user_response(l_buf.data, l_buf.cap, client);
  l_buf.idx += snprintf(l_buf.data + l_buf.idx, l_buf.cap - l_buf.idx,
			    "JOIN %s\r\n", channel->name);
  send_message_to_channel(&l_buf, channel->name, client, data);
  send_message_to_user(&l_buf, client->nickname, client, data);
  l_buf.idx = 0;
  send_user_list((t_btree *)channel->users, client, data, buffer);
  snprintf(buffer->data + buffer->idx, buffer->cap + 2, "\r\n");
  buffer->idx += 2;
  cb_write(&client->connect.write_buffer, buffer->data, buffer->idx);
  cb_write(&client->connect.write_buffer, buffer->data,
	   snprintf(buffer->data, BUFSIZE,
		    ":%s 366 %s %s :End of /NAMES list.\r\n",
		    data->server_name,
		    client->nickname,
		    channel->name));
}

static void		join_channel(char *chan,
				     t_client *client,
				     t_server_data *data)
{
  char		bufdata[BUFSIZE];
  t_buffer	buffer;
  t_channel	*channel;

  buffer.cap = BUFSIZE - 2;
  buffer.data = bufdata;
  write_user_list_reply_header(&buffer, client, data, chan);
  if (data->channels && (channel = strmap_get(data->channels, chan)) != NULL)
    {
      if (strmap_get(channel->users, client->nickname))
	return;
    }
  else if ((channel = malloc(sizeof(t_channel))))
    {
      memset(channel, 0, sizeof(t_channel));
      if ((channel->name = strdup(chan)) == NULL)
	{
	  free(channel);
	  send_message("Strdup failed", 400, client, data);
	  return;
	}
      strmap_insert(&data->channels, channel->name, channel);
    }
  if (channel)
    do_join_channel(channel, client, data, &buffer);
}

void			server_cmd_join(int argc,
					char **argv,
					t_client *client,
					t_server_data *data)
{
  char		*pch;

  if (!client->nickname)
    return;
  if (argc == 1)
    {
      pch = strtok(argv[0], ",");
      while (pch != NULL)
	{
	  join_channel(pch, client, data);
	  pch = strtok(NULL, ",");
	}
    }
  else
    send_message("JOIN <#channel, #channel ...>", 704, client, data);
}
