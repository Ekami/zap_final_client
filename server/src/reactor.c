/*
** reactor.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 05:02:38 2013 julien lebot
** Last update Sun Apr 28 07:21:32 2013 julien lebot
*/

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "socks.h"
#include "btree.h"
#include "circular_buffer.h"
#include "connect.h"
#include "reactor.h"

void			do_read(t_connection *item)
{
  char			buffer[BUFSIZE];
  int			bread;

  memset(buffer, 0, BUFSIZE);
  bread = read(item->fd, buffer, BUFSIZE);
  if (bread <= 0)
    return;
  cb_write(&item->read_buffer, buffer, bread);
  item->callbacks.read_cb(item, item->ctx);
}

void			do_write(t_connection *item)
{
  char			buffer[BUFSIZE];
  int			to_write;
  int			written;

  memset(buffer, 0, BUFSIZE);
  to_write = cb_read(&item->write_buffer, buffer, BUFSIZE);
  written = write(item->fd, buffer, to_write);
  if (written < to_write)
    cb_write(&item->write_buffer, buffer + written, to_write - written);
}

void			do_io(t_connection *item, fd_set *sets)
{
  long unsigned int	bread;

  bread = 0;
  if (item->fd < 0)
    return;
  if (FD_ISSET(item->fd, &sets[e_read_set]))
    {
      ioctl(item->fd, FIONREAD, &bread);
      if (bread == 0)
	{
	  item->closing = 1;
	  return;
	}
      do_read(item);
    }
  if (FD_ISSET(item->fd, &sets[e_write_set]))
    {
      item->callbacks.write_cb(item, item->ctx);
      if (!cb_is_empty(&item->write_buffer))
	do_write(item);
    }
}

void			clean_tree(t_connection *item, t_btree **tree)
{
  if (item->closing)
    {
      if (!cb_is_empty(&item->write_buffer))
	return;
      item->callbacks.disconnect_cb(item, item->ctx);
      close(item->fd);
      btree_remove_item(tree,
			item,
			(t_btree_cmp_fn)&compare_clients,
			NULL);
    }
}

void			do_work(t_btree **work_tree,
				int server_fd,
				void (*new_client_fn)(t_btree **, int, void *),
				void *ctx)
{
  int			max_fd;
  fd_set		sets[2];
  struct timeval	tv;

  max_fd = -1;
  tv.tv_sec = 0;
  tv.tv_usec = 100 * 1000;
  if (*work_tree != NULL)
    {
      btree_apply_suffix(*work_tree, (t_btree_apply_fn)&clean_tree, work_tree);
      if (*work_tree != NULL)
	max_fd = configure_sets(*work_tree, &sets[0]);
    }
  if (server_fd >= 0)
    {
      max_fd = MAX(max_fd, server_fd);
      FD_SET(server_fd, &sets[e_read_set]);
    }
  if (select(max_fd + 1, &sets[e_read_set], &sets[e_write_set], NULL, &tv) > 0)
    {
      if (FD_ISSET(server_fd, &sets[e_read_set]) && new_client_fn)
	new_client_fn(work_tree, server_fd, ctx);
      if (*work_tree != NULL)
	btree_apply_infix(*work_tree, (t_btree_apply_fn)&do_io, &sets[0]);
    }
}
