/*
** server_cmd_part.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 18:34:36 2013 julien lebot
** Last update Sun Apr 28 21:29:49 2013 julien lebot
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "server_commands.h"
#include "channel.h"
#include "send_helpers.h"

static int	channel_compare(t_channel *ch1, t_channel *ch2)
{
  if (ch1 == ch2)
    return (0);
  else
    return (-1);
}

static	void	send_part_msg(t_channel *chan,
			      t_client *client,
			      t_server_data *data)
{
  char		buffer[BUFSIZE];
  t_buffer	l_buf;

  l_buf.data = buffer;
  l_buf.cap = BUFSIZE;
  l_buf.idx = write_user_response(l_buf.data, l_buf.cap, client);
  l_buf.idx += snprintf(l_buf.data + l_buf.idx, l_buf.cap - l_buf.idx,
		   "PART %s\r\n", chan->name);
  send_message_to_channel(&l_buf, chan->name, client, data);
  send_message_to_user(&l_buf, client->nickname, client, data);
}

void		leave_channel(char *chan,
			      t_client *client,
			      t_server_data *data)
{
  char		buffer[BUFSIZE];
  t_channel	*channel;

  if (data->channels && (channel = strmap_get(data->channels, chan)))
    {
      if (strmap_get(channel->users, client->nickname))
	{
	  strmap_remove(&channel->users, client->nickname);
	  send_part_msg(channel, client, data);
	  dl_erase(&client->channels, channel, (t_dl_cmp_fn)&channel_compare);
	  if (channel->users == NULL)
	    {
	      strmap_remove(&data->channels, channel->name);
	      free(channel->name);
	      free(channel);
	    }
	}
      else
	  cb_write(&client->connect.write_buffer, buffer, snprintf(buffer, BUFSIZE,
			    ":%s 442 %s %s :You're not on that channel\r\n",
			    data->server_name, client->nickname, chan));
    }
}

void		server_cmd_part(int argc,
				char **argv,
				t_client *client,
				t_server_data *data)
{
  char		*pch;

  if (!client->nickname)
    return;
  if (argc == 1)
    {
      pch = strtok(argv[0], ",");
      while (pch != NULL)
	{
	  leave_channel(pch, client, data);
	  pch = strtok(NULL, ",");
	}
    }
  else
    send_message("PART :Not enough parameters", 461, client, data);
}
