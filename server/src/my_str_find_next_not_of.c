/*
** my_str_find_next_not_of.c for mylib in /home/lebot_j//afs/minishell
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Fri Nov  9 16:03:25 2012 julien lebot
** Last update Fri Apr 26 00:10:36 2013 julien lebot
*/

const char	*my_str_find_next_not_of(const char *str, char c)
{
  while (*str && *str == c)
    str++;
  return (str);
}
