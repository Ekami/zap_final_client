/*
** send_helpers.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 18:18:46 2013 julien lebot
** Last update Sun Apr 28 17:56:34 2013 julien lebot
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "send_helpers.h"
#include "circular_buffer.h"

void		send_message(char *msg,
			     int code,
			     t_client *client,
			     t_server_data *ctx)
{
  char		buffer[BUFSIZE];
  int		size;

  size = snprintf(buffer,
		  BUFSIZE,
		  ":%s %3d %s :%s\r\n",
		  ctx->server_name,
		  code,
		  client->nickname ? client->nickname : "",
		  msg);
  cb_write(&client->connect.write_buffer, buffer, size);
}

int		write_user_response(char *msg,
				   int size,
				   t_client *client)
{
  return (snprintf(msg,
		   size,
		   ":%s!~%s@%s ",
		   client->nickname,
		   client->hostname,
		   client->ipaddress));
}
