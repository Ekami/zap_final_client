/*
** server_accept_client.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 18:27:48 2013 julien lebot
** Last update Sun Apr 28 22:00:14 2013 julien lebot
*/

#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "connect.h"
#include "server_callbacks.h"

static	void			init_callbacks_and_fd(t_callbacks *cbs, int *fd)
{
  cbs->read_cb = (callback_fn)&read_cb;
  cbs->write_cb = (callback_fn)&write_cb;
  cbs->disconnect_cb = (callback_fn)&disconnect_cb;
  *fd = 0;
}

void				new_client(t_btree **work_tree,
					   int fd,
					   void *data)
{
  struct sockaddr_storage	addr;
  socklen_t			socklen;
  t_client			*client;
  t_callbacks			cbs;
  int				cfd;
  
  init_callbacks_and_fd(&cbs, &cfd);
  if ((client = malloc(sizeof(t_client))) != NULL)
    {
      memset(client, 0, sizeof(t_client));
      socklen = sizeof(addr);
      if ((cfd = accept(fd, (struct sockaddr *)&addr, &socklen)) < 0)
	perror("accept");
      else if (parse_ipaddress(&addr, client->ipaddress, IPADDR_SIZE) < 0)
	perror("inet_ntop");
      else if (init_connection(&client->connect, cfd, cbs, data) < 0)
	perror("init_connection");
      else
	{
	  btree_insert_data(work_tree, client, (t_btree_cmp_fn)&compare_clients);
	  return ((void)printf("New client connected [%s]\n", client->ipaddress));
	}
    }
  (void)close(cfd);
  free(client);
}
