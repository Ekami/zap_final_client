/*
** server_main.c for server in /home/nicopo_v//Documents/svn/myirc-2016-nicopo_v/src
** 
** Made by vincent nicopolsky
** Login   <nicopo_v@epitech.net>
** 
** Started on  Thu Apr 25 00:49:26 2013 vincent nicopolsky
** Last update Sun Apr 28 20:54:57 2013 julien lebot
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include "socks.h"
#include "reactor.h"
#include "connect.h"
#include "connection.h"
#include "server_data_context.h"
#include "server_callbacks.h"
#include "send_helpers.h"
#include "channel.h"
#include "btree.h"
#include "server_cleanup.h"
#include "print_server_usage.h"
#include "string_utils.h"

t_server_data		g_server_data;

void		signal_handler(int sig)
{
  if (sig == SIGTERM ||
      sig == SIGINT)
    {
      printf("\b\bServer shutting down\n");
      g_server_data.closing = 1;
    }
}

int		get_server_created_time(t_server_data *data)
{
  char		*time_str;
  struct tm	*newtime;
  time_t	ltime;

  if (time(&ltime) < 0)
    perror("time");
  else if ((newtime = localtime(&ltime)) == NULL)
    perror("localtime");
  else if ((time_str = asctime(newtime)) == NULL)
    perror("asctime");
  else
    {
      strncpy(data->created, time_str, 26);
      *(char *)my_str_find_first_of(data->created, '\n') = '\0';
      return (0);
    }
  return (1);
}

int		start_server(int port)
{
  g_server_data.server_port = port;
  g_server_data.closing = 0;
  signal(SIGTERM, &signal_handler);
  signal(SIGINT, &signal_handler);
  if ((g_server_data.server_fd =
       new_sock_server(port, g_server_data.ipaddress, IPADDR_SIZE)) < 0)
    perror("create_socket");
  else if (gethostname(g_server_data.server_name, HOST_NAME_MAX) < 0)
    perror("gethostname");
  else if (listen(g_server_data.server_fd, 10) < 0)
    perror("listen");
  else if (get_server_created_time(&g_server_data) >= 0)
    {
      g_server_data.server_name_size = strlen(g_server_data.server_name);
      g_server_data.work_tree = NULL;
      g_server_data.channels = NULL;
      while (!g_server_data.closing)
	do_work(&g_server_data.work_tree, g_server_data.server_fd,
		&new_client, &g_server_data);
      do_cleanup(&g_server_data);
      return (0);
    }
  return (1);
}
/*
      else if ((server = new_sock_server(port)) < 0)
	{
	  perror("server");
	  return (1);
	}
 */
int	main(int argc, char **argv)
{
  int	port;

  if (argc == 2)
    {
      port = atoi(argv[1]);
      if (port < 1024 || port > 65000)
	{
	  fprintf(stderr,
		  "This server only support ports between 1024 and 65000\n");
	  return (1);
	}
      else
	start_server(port);
    }
  else
    print_usage();
  return (0);
}
