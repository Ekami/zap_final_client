/*
** btree_apply_suffix.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 09:25:47 2012 julien lebot
** Last update Sat Apr 27 14:28:19 2013 julien lebot
*/

#include "btree.h"

void	btree_apply_suffix(t_btree *root,
			   t_btree_apply_fn applyfn,
			   void *ctx)
{
  if (root->left)
    btree_apply_suffix(root->left, applyfn, ctx);
  if (root->right)
    btree_apply_suffix(root->right, applyfn, ctx);
  applyfn(root->data, ctx);
}
