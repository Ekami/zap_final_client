/*
** parse_ipaddress.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 21:01:20 2013 julien lebot
** Last update Sun Apr 28 21:01:39 2013 julien lebot
*/

#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "connect.h"

int				parse_ipaddress(struct sockaddr_storage *addr,
						char *ipaddress,
						int size)
{
  int				retval;
  struct sockaddr_in		*sa;
  struct sockaddr_in6		*sa6;

  if (addr->ss_family == AF_INET)
    {
      sa = (struct sockaddr_in *)addr;
      retval = (inet_ntop(addr->ss_family, &sa->sin_addr,
			  ipaddress, size) == NULL);
    }
  else
    {
      sa6 = (struct sockaddr_in6 *)addr;
      retval = (inet_ntop(addr->ss_family, &sa6->sin6_addr,
			  ipaddress, size) == NULL);
    }
  if (retval)
    return (-1);
  else
    return (0);
}
