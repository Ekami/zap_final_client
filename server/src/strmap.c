/*
** strmap.c for  in /home/lebot_j//Tek2/myftp-2015s-2016-2017si-lebot_j
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 13 21:32:35 2013 julien lebot
** Last update Sun Apr 28 14:41:24 2013 julien lebot
*/

#include <stdlib.h>
#include <string.h>
#include "strmap.h"
#include "btree.h"

int	strmap_cmp(t_str_key_value *kvp1, t_str_key_value *kvp2)
{
  return (strcmp(kvp1->key, kvp2->key));
}

int	strmap_insert(struct s_strtree **tree,
		      char const *key,
		      void *value)
{
  t_str_key_value *kvp;

  if ((kvp = malloc(sizeof(t_str_key_value))) == NULL)
    return (0);
  kvp->key = key;
  kvp->value = value;
  return (btree_insert_data((t_btree **)tree,
			    kvp,
			    (t_btree_cmp_fn)&strmap_cmp));
}

void	*strmap_get(struct s_strtree *tree,
		    char const *key)
{
  t_str_key_value kvp;
  t_str_key_value *result;

  kvp.key = key;
  kvp.value = NULL;
  result = btree_search_item((t_btree *)tree, &kvp,
			     (t_btree_cmp_fn)&strmap_cmp);
  if (result == NULL)
    return (NULL);
  else
    return (result->value);
}

void	strmap_destroy(struct s_strtree **tree)
{
  btree_free((t_btree *)*tree, &free);
  *tree = NULL;
}
