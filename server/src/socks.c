/*
** ftp_socks.c for  in /home/lebot_j//Dev/myftp-2015s-2016-2017si-lebot_j
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr  9 03:18:04 2013 julien lebot
** Last update Sun Apr 28 20:57:15 2013 julien lebot
*/

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "connection.h"

int			new_sock_client(char const *ip, int port)
{
  int			fd;
  struct hostent	*host;
  struct sockaddr_in	sock_in;

  memset(&sock_in, 0, sizeof(struct sockaddr_in));
  host = gethostbyname(ip);
  if (host == NULL)
    {
      errno = EFAULT;
      return (-1);
    }
  sock_in.sin_family = AF_INET;
  sock_in.sin_port = htons(port);
  sock_in.sin_addr.s_addr = ((struct in_addr *) (host->h_addr))->s_addr;
  if ((fd = socket(host->h_addrtype, SOCK_STREAM, 0)) > 0)
    {
      if (connect(fd, (struct sockaddr *)&sock_in, sizeof(sock_in)) < 0)
	{
	  close(fd);
	  return (-1);
	}
    }
  return (fd);
}

int				new_sock_server(int port,
						char *ipaddress,
						int size)
{
  int				fd;
  struct sockaddr_storage	sockaddr;
  struct sockaddr_in		*sock_in;
  int				yes;

  yes = 1;
  memset(&sockaddr, 0, sizeof(sockaddr));
  sock_in = (struct sockaddr_in *)&sockaddr;
  sock_in->sin_family = AF_INET;
  sock_in->sin_port = htons(port);
  sock_in->sin_addr.s_addr = htonl(INADDR_ANY);
  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) > 0)
    {
      if ((setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) ||
	  (bind(fd, (struct sockaddr*)sock_in, sizeof(*sock_in)) < 0) ||
	  parse_ipaddress(&sockaddr, ipaddress, size) < 0)
	{
	  close(fd);
	  return (-1);
	}
    }
  return (fd);
}
