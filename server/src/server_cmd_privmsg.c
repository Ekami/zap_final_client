/*
** server_cmd_privmsg.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:43:49 2013 julien lebot
** Last update Sun Apr 28 04:36:43 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server_commands.h"
#include "send_helpers.h"

void		server_cmd_privmsg(int argc,
				   char **argv,
				   t_client *client,
				   t_server_data *data)
{
  char		buffer[BUFSIZE];
  t_buffer	l_buf;

  if (!client->nickname)
    return;
  if (argc == 2)
    {
      l_buf.data = buffer;
      l_buf.cap = BUFSIZE;
      l_buf.idx = write_user_response(l_buf.data, l_buf.cap, client);
      l_buf.idx += snprintf(l_buf.data + l_buf.idx, l_buf.cap - l_buf.idx,
		       "PRIVMSG %s :%s\r\n", argv[0], argv[1]);
      if (argv[0][0] == '#')
	send_message_to_channel(&l_buf, argv[0], client, data);
      else
	send_message_to_user(&l_buf, argv[0], client, data);
    }
  else
    send_message("PRIVMSG <msgtarget> :<message> (RFC 1459)",
		 704, client, data);
}
