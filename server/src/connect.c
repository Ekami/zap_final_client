/*
** connect.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 13:18:12 2013 julien lebot
** Last update Thu Apr 25 23:54:50 2013 julien lebot
*/

#include <stdio.h>
#include <stdlib.h>
#include "connect.h"
#include "connection.h"
#include "socks.h"

t_connection	*client_connect(t_endpoint c,
				t_callbacks cbs,
				void *ctx)
{
  int	fd;
  t_connection	*connect;

  if ((fd = new_sock_client(c.address, c.port)) > 0)
    {
      if (((connect = malloc(sizeof(t_connection))) != NULL) &&
	  init_connection(connect, fd, cbs, ctx) >= 0)
	return (connect);
      free(connect);
    }
  return (NULL);
}
