/*
** btree_apply_prefix.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 09:21:47 2012 julien lebot
** Last update Tue Apr 23 13:56:50 2013 julien lebot
*/

#include "btree.h"

void	btree_apply_prefix(t_btree *root,
			   t_btree_apply_fn applyf,
			   void *ctx)
{
  applyf(root->data, ctx);
  if (root->left)
    btree_apply_prefix(root->left, applyf, ctx);
  if (root->right)
    btree_apply_prefix(root->right, applyf, ctx);
}
