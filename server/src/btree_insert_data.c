/*
** btree_insert_data.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 09:32:37 2012 julien lebot
** Last update Sun Apr 28 02:00:46 2013 julien lebot
*/

#include "btree.h"

int		btree_insert_data(t_btree **root,
				  void *data,
				  int (*cmpf)(void *, void *))
{
  if (!(*root))
    {
      *root = btree_create_node(data);
      return (*root != NULL);
    }
  else if (cmpf((*root)->data, data) > 0)
    {
      if ((*root)->left)
	btree_insert_data(&(*root)->left, data, cmpf);
      else
	(*root)->left = btree_create_node(data);
      return ((*root)->left != NULL);
    }
  else
    {
      if ((*root)->right)
	btree_insert_data(&(*root)->right, data, cmpf);
      else
	(*root)->right = btree_create_node(data);
      return ((*root)->right != NULL);
    }
  return (0);
}

int		btree_insert_branch(t_btree *root,
				    t_btree *branch,
				    int (*cmpf)(void *, void *))
{
  if (!root)
    return (root != NULL);
  else if (cmpf(root->data, branch->data) > 0)
    {
      if (root->left)
	btree_insert_branch(root->left, branch, cmpf);
      else
	root->left = branch;
      return (root->left != NULL);
    }
  else
    {
      if (root->right)
	btree_insert_branch(root->right, branch, cmpf);
      else
	root->right = branch;
      return (root->right != NULL);
    }
  return (0);
}
