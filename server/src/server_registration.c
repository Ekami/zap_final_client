/*
** server_registration.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 17:00:42 2013 julien lebot
** Last update Sun Apr 28 21:04:12 2013 julien lebot
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "send_helpers.h"
#include "circular_buffer.h"

void	send_notice(t_client *client,
		    t_server_data *ctx)
{
  char		buffer[BUFSIZE];
  int		size;

  size = snprintf(buffer,
		  BUFSIZE,
		  ":%s NOTICE * :You will give those students a good grade\r\n",
		  ctx->server_name);
  cb_write(&client->connect.write_buffer, buffer, size);
  client->client_state = 42;
}

void		send_welcome_message(t_client *c,
				     t_server_data * ctx)
{
  char		buffer[BUFSIZE];
  int		size;

  size = snprintf(buffer,
		  BUFSIZE,
		  ":%s 001 %s :Welcome to MyIRC::Server %s!%s@%s\r\n",
		  ctx->server_name,
		  c->nickname,
		  c->nickname,
		  c->realname,
		  c->hostname);
  cb_write(&c->connect.write_buffer, buffer, size);
  c->client_state = RPL_YOURHOST;
}

void		send_host_message(t_client *c,
				  t_server_data *ctx)
{
  char		buffer[BUFSIZE];
  int		size;

  size = snprintf(buffer,
		  BUFSIZE,
		  ":%s 002 %s :Your host is %s[%s/%d], running MyIRC (v42)\r\n",
		  ctx->server_name,
		  c->nickname,
		  ctx->server_name,
		  ctx->ipaddress,
		  ctx->server_port);
  cb_write(&c->connect.write_buffer, buffer, size);
  c->client_state = RPL_CREATED;
}

void		send_server_created(t_client *c,
				    t_server_data *ctx)
{
  char		buffer[BUFSIZE];
  int		size;

  size = snprintf(buffer,
		  BUFSIZE,
		  ":%s 003 %s :This server was created %s\r\n",
		  ctx->server_name,
		  c->nickname,
		  ctx->created);
  cb_write(&c->connect.write_buffer, buffer, size);
  c->client_state = RPL_MYINFO;
}

void		send_server_info(t_client *c,
				 t_server_data *ctx)
{
  char		buffer[BUFSIZE];
  int		size;

  size = snprintf(buffer,
		  BUFSIZE,
		  ":%s 004 %s :%s v42  \r\n",
		  ctx->server_name,
		  c->nickname,
		  ctx->server_name);
  cb_write(&c->connect.write_buffer, buffer, size);
  c->client_state++;
}
