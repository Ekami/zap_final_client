/*
** server_cmd_nick.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:40:13 2013 julien lebot
** Last update Sun Apr 28 03:40:54 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server_commands.h"
#include "send_helpers.h"

void		server_cmd_nick(int argc,
				char **argv,
				t_client *c,
				t_server_data *data)
{
  if (argc == 1)
    {
      c->nickname = strdup(argv[0]);
      if (c->nickname)
	c->client_state = 1;
      else
	send_message("Strdup failed", 400, c, data);
    }
  else
    send_message("NICK <nickname> (RFC 2812)", 704, c, data);
}
