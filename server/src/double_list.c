/*
** double_list.c for  in /home/lebot_j//afs/myselect-2016ed-2015s-2017si-lebot_j
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Nov 25 22:37:55 2012 julien lebot
** Last update Sun Apr 28 05:02:55 2013 julien lebot
*/

#include <stdlib.h>
#include "double_list.h"

t_double_list	*dl_insert(t_double_list **list, void *item)
{
  if (item && *list)
    dl_insert_after(*list, item);
  else if (item && (*list = malloc(sizeof(t_double_list))))
    {
      (*list)->data = item;
      (*list)->next = NULL;
      (*list)->prev = NULL;
    }
  return (*list);
}

t_double_list	*dl_insert_after(t_double_list *after, void *item)
{
  t_double_list	*curt;

  curt = NULL;
  if (item && (curt = malloc(sizeof(t_double_list))))
    {
      curt->data = item;
      curt->next = after->next;
      after->next = curt;
      curt->prev = after;
    }
  return (curt);
}

void		dl_erase(t_double_list **list, void *item, t_dl_cmp_fn fn)
{
  t_double_list	*save;
  t_double_list	*what;

  what = *list;
  while (what && fn(what->data, item) != 0)
    what = what->next;
  if (!what)
    return;
  save = what->next;
  if (what->prev)
    what->prev->next = what->next;
  if (what->next)
  what->next->prev = what->prev;
  if (what == *list)
    *list = save;
  if (what == save)
    free(what);
  else
    {
      free(what);
      what = save;
    }
}

void		dl_free(t_double_list **list, void (*free_fn)(void *))
{
  t_double_list	*temp;
  t_double_list *next;

  temp = *list;
  while (temp)
    {
      next = temp->next;
      if (free_fn)
	free_fn(temp->data);
      free(temp);
      temp = next;
    }
  *list = NULL;
}

t_double_list	*dl_last(t_double_list *list)
{
  while (list->next)
    list = list->next;
  return (list);
}
