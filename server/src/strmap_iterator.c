/*
** strmap_iterator.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 16:55:04 2013 julien lebot
** Last update Sat Apr 27 17:54:25 2013 julien lebot
*/

#include "btree.h"
#include "strmap.h"

/*
** This struct is ever only used within
** this compilation unit. EVER.
*/
typedef	struct		s_strmap_iterator_ctx
{
  t_strtree_apply_fn	apply_fn;
  void			*ctx;
}			t_strmap_iterator_ctx;

static	void		strmap_iterator(t_str_key_value *kvp, void *ctx)
{
  t_strmap_iterator_ctx	*it;

  it = (t_strmap_iterator_ctx *)ctx;
  it->apply_fn(kvp->key, kvp->value, it->ctx);
}

void			strmap_apply(struct s_strtree *tree,
				     t_strtree_apply_fn fn,
				     void *user_ctx)
{
  t_strmap_iterator_ctx	ctx;

  ctx.apply_fn = fn;
  ctx.ctx = user_ctx;
  btree_apply_infix((t_btree *)tree,
		    (t_btree_apply_fn)strmap_iterator,
		    &ctx);
}
