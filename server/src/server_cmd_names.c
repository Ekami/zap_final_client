/*
** server_cmd_names.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 05:05:41 2013 julien lebot
** Last update Sun Apr 28 07:30:27 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server_commands.h"
#include "send_helpers.h"
#include "channel.h"

static	void		print_user_in_channel(t_btree *usr_node,
					       t_client *client,
					       t_server_data *data,
					       t_buffer *buf)
{
  t_str_key_value	*kvp;

  if (usr_node->left)
    print_user_in_channel(usr_node->left, client, data, buf);
  if (usr_node->data)
    {
      kvp = (t_str_key_value *)usr_node->data;
      if ((int)strlen(kvp->key) < buf->cap - buf->idx)
	{
	  if (buf->data[buf->idx] == ':')
	    buf->idx += snprintf(buf->data + buf->idx, buf->cap - buf->idx,
				 "%s", kvp->key);
	  else
	    buf->idx += snprintf(buf->data + buf->idx, buf->cap - buf->idx,
				 " %s", kvp->key);
	}
    }
  if (usr_node->right)
    print_user_in_channel(usr_node->right, client, data, buf);
}

static	void		print_users_in_channel(t_channel *chan,
					       t_client *client,
					       t_server_data *data)
{
  char			buffer[BUFSIZE];
  t_buffer		l_buf;

  l_buf.data = buffer;
  l_buf.cap = BUFSIZE - 2;
  l_buf.idx = snprintf(l_buf.data, l_buf.cap, ":%s 353 %s = %s :",
		       data->server_name, client->nickname, chan->name);
  print_user_in_channel((t_btree *)chan->users, client, data, &l_buf);
  l_buf.idx += snprintf(l_buf.data + l_buf.idx, 3, "\r\n");
  cb_write(&client->connect.write_buffer, l_buf.data, l_buf.idx);
}

static	void		print_users_in_channels(t_btree *ch_node,
						char *chan_names,
						t_client *client,
						t_server_data *data)
{
  t_channel		*chan;
  int			can_print;

  can_print = 1;
  if (ch_node->left)
    print_users_in_channels(ch_node->left, chan_names, client, data);
  if (ch_node->data)
    {
      chan = (t_channel *)((t_str_key_value *)ch_node->data)->value;
      if (chan_names && strstr(chan->name, chan_names) == NULL)
	can_print = 0;
      if (chan->users && can_print)
	print_users_in_channel(chan, client, data);
    }
  if (ch_node->right)
    print_users_in_channels(ch_node->right, chan_names, client, data);
}

static	void		print_user_not_in_channel(t_btree *node,
						  t_buffer *buf)
{
  t_client		*l_cl;

  if (node->left)
    print_user_not_in_channel(node->left, buf);
  if (node->data)
    {
      l_cl = (t_client *)node->data;
      if (l_cl->channels == NULL &&
	  ((int)strlen(l_cl->nickname) < buf->cap - buf->idx))
	{
	  if (buf->data[buf->idx] == ':')
	    buf->idx += snprintf(buf->data + buf->idx, buf->cap - buf->idx,
				 "%s", l_cl->nickname);
	  else
	    buf->idx += snprintf(buf->data + buf->idx, buf->cap - buf->idx,
				 " %s", l_cl->nickname);
	}
    }
  if (node->right)
    print_user_not_in_channel(node->right, buf);
}

void			server_cmd_names(int argc,
					 char **argv,
					 t_client *client,
					 t_server_data *data)
{
  char			buffer[BUFSIZE];
  t_buffer		l_buf;

  if (!client->nickname)
    return;
  if (argc != 1)
    {
      l_buf.data = buffer;
      l_buf.cap = BUFSIZE - 2;
      l_buf.idx = snprintf(l_buf.data, l_buf.cap, ":%s 353 %s * * :",
			   data->server_name, client->nickname);
      print_user_not_in_channel(data->work_tree, &l_buf);
      l_buf.idx += snprintf(l_buf.data + l_buf.idx, 3, "\r\n");
      cb_write(&client->connect.write_buffer, l_buf.data, l_buf.idx);
    }
  if (data->channels != NULL)
    print_users_in_channels((t_btree *)data->channels,
			    argc == 1 ? argv[0] : NULL, client, data);
  cb_write(&client->connect.write_buffer, buffer,
	   snprintf(buffer, BUFSIZE,
		    ":%s 366 * %s :End of /NAMES list.\r\n", data->server_name,
		    client->nickname));
}
