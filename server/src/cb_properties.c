/*
** cb_properties.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 11:52:28 2013 julien lebot
** Last update Fri Apr 26 22:50:17 2013 julien lebot
*/

#include "circular_buffer.h"

int		cb_is_full(t_circular_buffer *cb)
{
  return ((cb->end + 1) % cb->num == cb->start);
}

int		cb_is_empty(t_circular_buffer *cb)
{
  return (cb->end == cb->start);
}

void		cb_set_empty(t_circular_buffer *cb)
{
  cb->start = 0;
  cb->end   = 0;
}

int		cb_size(t_circular_buffer *cb)
{
  if (cb->end > cb->start)
    return ((int)(cb->end - cb->start));
  else
    return ((int)(cb->start - cb->end));
}

