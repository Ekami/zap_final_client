/*
** btree_apply_by_level.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 13:52:58 2012 julien lebot
** Last update Wed Apr 24 01:45:29 2013 julien lebot
*/

#include <stdlib.h>
#include "btree.h"

void		apply_by_level(t_btree *root,
			       void (*applyf)(void *item,
					      int current_level,
					      int is_first_elem),
			       int lvl,
			       int first)
{
  applyf(root->data, lvl, first);
  if (root->left)
    apply_by_level(root->left, applyf, lvl + 1, first);
  if (root->right)
    apply_by_level(root->right, applyf, lvl + 1, root->left ? 0 : 1);
}

void		btree_apply_by_level(t_btree *root,
				     void (*applyf)(void *item,
						    int current_level,
						    int is_first_elem))
{
  apply_by_level(root, applyf, 0, 1);
}
