/*
** create_work_item.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 13:14:50 2013 julien lebot
** Last update Sun Apr 28 07:17:04 2013 julien lebot
*/

#include <stdlib.h>
#include "connection.h"
#include "circular_buffer.h"
#include "server_data_context.h"

int	init_connection(t_connection *connect,
			int fd,
			t_callbacks cbs,
			void *ctx)
{
  connect->closing = 0;
  connect->fd = fd;
  connect->callbacks = cbs;
  connect->ctx = ctx;
  if (cb_init(&connect->write_buffer, BUFSIZE, BUF_NUM) < 0)
    return (-1);
  if (cb_init(&connect->read_buffer, BUFSIZE, BUF_NUM) < 0)
    {
      cb_free(&connect->write_buffer);
      return (-1);
    }
  return (0);
}

void	free_connection(t_connection *connect)
{
  cb_free(&connect->write_buffer);
  cb_free(&connect->read_buffer);
}

int		compare_clients(void *a, void *b)
{
  return (compare_connections(&((t_client *)a)->connect,
			      &((t_client *)b)->connect));
}

int		compare_connections(void *a, void *b)
{
  return (((t_connection *)a)->fd < ((t_connection *)b)->fd);
}
