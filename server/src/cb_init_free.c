/*
** cb_init_free.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 11:50:32 2013 julien lebot
** Last update Sun Apr 28 07:30:51 2013 julien lebot
*/

#include <stdlib.h>
#include <string.h>
#include "circular_buffer.h"

static	void	cb_free_from(t_circular_buffer *cb, int pos)
{
  int		i;

  i = 0;
  while (i < pos)
    free(cb->elems[i++].data);
}

int	cb_init(t_circular_buffer *cb, int size, int num)
{
  int	i;

  i = 0;
  if (num <= 0 || size <= 0)
    return (-1);
  cb->num = num + 1;
  cb->size  = size;
  cb->start = 0;
  cb->end   = 0;
  if ((cb->elems = malloc(cb->num * sizeof(*cb->elems))) == NULL)
    return (-1);
  while (i <= num)
    {
      cb->elems[i].data = malloc(cb->size);
      if (!cb->elems[i].data)
	{
	  cb_free_from(cb, i);
	  free(cb->elems);
	  return (-1);
	}
      memset(cb->elems[i].data, 0, sizeof(cb->size));
      cb->elems[i].size = 0;
      i++;
    }
  return (0);
}

void	cb_free(t_circular_buffer *cb)
{
  cb_free_from(cb, cb->num);
  free(cb->elems);
}
