/*
** server_cleanup.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 15:01:29 2013 julien lebot
** Last update Sun Apr 28 15:08:25 2013 julien lebot
*/

#include <stdlib.h>
#include <unistd.h>
#include "server_cleanup.h"
#include "channel.h"
#include "btree.h"

static void	free_client(t_client *client)
{
  if (client)
    {
      client->connect.callbacks.disconnect_cb(&client->connect,
					      client->connect.ctx);
      close(client->connect.fd);
    }
  free(client);
}

static void	free_channel(t_str_key_value *kvp)
{
  t_channel	*chan;

  if (kvp)
    {
      chan = (t_channel *)kvp;
      free(chan->name);
      btree_free((t_btree *)chan->users, &free);
      free(chan);
    }
}

void		do_cleanup(t_server_data *data)
{
  if (data)
    {
      btree_free(data->work_tree, (void (*)(void *))&free_client);
      data->work_tree = NULL;
      btree_free((t_btree *)data->channels,
		 (void (*)(void *))&free_channel);
      data->channels = NULL;
    }
}
