/*
** my_str_find_first_of.c for mylib in /home/lebot_j//afs/minishell
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Fri Nov  9 16:00:36 2012 julien lebot
** Last update Fri Apr 26 00:22:48 2013 julien lebot
*/

const char	*my_str_find_first_of(const char *str, char what)
{
  const char	*backup;

  backup = str;
  while (*str && *str != what)
    ++str;
  if (*str == what)
    return (str);
  else
    return (backup);
}
