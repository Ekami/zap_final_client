/*
** strmap_remove.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 18:54:05 2013 julien lebot
** Last update Sun Apr 28 00:55:22 2013 julien lebot
*/

#include <string.h>
#include <stdlib.h>
#include "btree.h"
#include "strmap.h"

void	strmap_remove(struct s_strtree **tree,
		      char const *key)
{
  t_str_key_value kvp;

  kvp.key = key;
  kvp.value = NULL;
  btree_remove_item((t_btree **)tree,
		    &kvp,
		    (t_btree_cmp_fn)&strmap_cmp,
		    (void (*)(void *))&free);
}
