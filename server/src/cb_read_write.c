/*
** cb_read_write.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 11:51:39 2013 julien lebot
** Last update Sat Apr 27 10:42:54 2013 julien lebot
*/

#include <string.h>
#include "circular_buffer.h"

#ifndef		MIN
# define	MIN(a, b)	((a) < (b) ? (a) : (b))
#endif

int	cb_write(t_circular_buffer *cb, char *elem, int size)
{
  int	data_size;

  data_size = MIN(size, cb->size);
  memcpy(cb->elems[cb->end].data, elem, data_size);
  cb->elems[cb->end].size = data_size;
  cb->end = (cb->end + 1) % cb->num;
  if (cb->end == cb->start)
    cb->start = (cb->start + 1) % cb->num;
  return (data_size);
}

int	cb_read(t_circular_buffer *cb, char *elem, int size)
{
  int	data_size;

  data_size = MIN(size, cb->elems[cb->start].size);
  memcpy(elem, cb->elems[cb->start].data, data_size);
  cb->start = (cb->start + 1) % cb->num;
  return (data_size);
}
