/*
** server_callbacks.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:48:56 2013 julien lebot
** Last update Sun Apr 28 18:16:46 2013 julien lebot
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "server_data_context.h"
#include "string_utils.h"
#include "server_cmd.h"
#include "channel.h"
#include "send_helpers.h"
#include "server_commands.h"

static t_server_cmd	g_cmds[] =
  {
    { "USER", &server_cmd_user },
    { "NICK", &server_cmd_nick },
    { "JOIN", &server_cmd_join },
    { "PART", &server_cmd_part },
    { "PRIVMSG", &server_cmd_privmsg},
    { "LIST", &server_cmd_list },
    { "NAMES", &server_cmd_names },
    { "QUIT", &server_cmd_quit }
  };

static	t_client_state_fn	g_client_states[] =
  {
    { 0, &send_notice },
    { RPL_WELCOME, &send_welcome_message },
    { RPL_YOURHOST, &send_host_message },
    { RPL_CREATED, &send_server_created },
    { RPL_MYINFO, &send_server_info }
  };

static void	remove_trailing_crlf(char *str)
{
  char		*cr;

  cr = (char *)my_str_find_first_of(str, '\r');
  if (*cr == '\r')
    *cr = '\0';
}

static void	parse_client_input(char *buffer,
				   t_client *c,
				   t_server_data *ctx)
{
  size_t	i;
  int		cmdlen;
  int		argc;
  char		**argv;
  char		*sepstr;

  i = 0;
  while (i < sizeof(g_cmds) / sizeof(g_cmds[0]))
    {
      cmdlen = strlen(g_cmds[i].cmd);
      if (strncmp(buffer, g_cmds[i].cmd, cmdlen - 1) == 0)
	{
	  sepstr = (char *)my_str_find_first_of(buffer, ':');
	  argc = my_str_to_wordtab(buffer + cmdlen, &argv);
	  if (*sepstr == ':')
	    argv[argc - 1] = strdup(sepstr + 1);
	  g_cmds[i].fn(argc, argv, c, ctx);
	  free_wordtab(argv);
	  return;
	}
      i++;
    }
  if (i >= sizeof(g_cmds) / sizeof(g_cmds[0]))
    send_message("unknown command", 421, c, ctx);
}

void		read_cb(t_client *c, t_server_data *ctx)
{
  char		buffer[BUFSIZE];

  memset(buffer, 0, BUFSIZE);
  cb_read(&c->connect.read_buffer, buffer, BUFSIZE);
  remove_trailing_crlf(buffer);
  parse_client_input(buffer, c, ctx);
}

void		write_cb(t_client *c, t_server_data * ctx)
{
  size_t	i;

  i = 0;
  while (i < sizeof(g_client_states) / sizeof(g_client_states[0]))
    {
      if (c->client_state == g_client_states[i].value)
	{
	  g_client_states[i].fn(c, ctx);
	}
      i++;
    }
}

void		disconnect_cb(t_client *c, t_server_data *ctx)
{
  t_double_list	*chans;

  (void)ctx;
  printf("Client disconnected\n");
  chans = c->channels;
  while (chans)
    {
      leave_channel(((t_channel *)chans->data)->name, c, ctx);
      chans = chans->next;
    }
  c->client_state = -1;
  free(c->nickname);
  free(c->realname);
  free(c->hostname);
  free_connection(&c->connect);
  dl_free(&c->channels, &free);
}
