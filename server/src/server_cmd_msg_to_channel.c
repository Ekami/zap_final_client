/*
** server_cmd_msg_to_channel.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:36:19 2013 julien lebot
** Last update Sun Apr 28 04:55:48 2013 julien lebot
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "btree.h"
#include "channel.h"
#include "server_commands.h"
#include "send_helpers.h"

static void	do_send_message_to_channel(t_buffer *message,
					   t_btree *chan_user,
					   t_client *client,
					   t_server_data *data)
{
  t_client	*recipient;

  if (!chan_user)
    return;
  if (chan_user->left)
    do_send_message_to_channel(message, chan_user->left, client, data);
  if (chan_user->data)
    {
      recipient = (t_client *)((t_str_key_value *)chan_user->data)->value;
      if (recipient != client)
	cb_write(&recipient->connect.write_buffer, message->data, message->idx);
    }
  if (chan_user->right)
    do_send_message_to_channel(message, chan_user->right, client, data);
}

void		send_message_to_channel(t_buffer *message,
					char *chan,
					t_client *client,
					t_server_data *data)
{
  char		buffer[BUFSIZE];
  t_double_list	*chans;

  chans = client->channels;
  while (chans && strcmp(((t_channel *)chans->data)->name, chan) != 0)
    chans = chans->next;
  if (!chans)
    {
      cb_write(&client->connect.write_buffer, buffer,
	       snprintf(buffer, BUFSIZE,
			":%s 442 %s %s :You're not on that channel\r\n",
			data->server_name, client->nickname, chan));
    }
  else
    {
      do_send_message_to_channel(message,
				 (t_btree *)((t_channel *)chans->data)->users,
				 client,
				 data);
    }
}
