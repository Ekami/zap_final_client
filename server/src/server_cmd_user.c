/*
** server_cmd_user.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:38:41 2013 julien lebot
** Last update Sun Apr 28 21:58:05 2013 julien lebot
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include "server_commands.h"
#include "send_helpers.h"

void			server_cmd_user(int argc,
					char **argv,
					t_client *c,
					t_server_data *data)
{
  char			*usage;

  usage = "Syntax: USER <user> <mode> <unused> :<realname> (RFC 2812)";
  if (c->hostname)
    send_message("You may not reregister", 462, c, data);
  else if (argc == 4)
    {
      c->hostname = strdup(argv[0]);
      c->realname = strdup(argv[3]);
      if (!c->hostname || !c->realname)
	{
	  free(c->hostname);
	  free(c->realname);
	  send_message("Strdup failed", 400, c, data);
	}
      printf("%s (%s) connected\n", c->hostname, c->realname);
    }
  else
    send_message(usage, 704, c, data);
}
