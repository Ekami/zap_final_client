/*
** btree_level_count.c for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 10:39:09 2012 julien lebot
** Last update Tue Apr 23 13:55:55 2013 julien lebot
*/

#include "btree.h"

int	btree_level_count(t_btree *root)
{
  if (root)
    return (1 + MAX(btree_level_count(root->left),
		    btree_level_count(root->right)));
  else
    return (0);
}
