/*
** print_server_usage.c for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 16:36:12 2013 julien lebot
** Last update Sun Apr 28 16:43:21 2013 julien lebot
*/

#include <stdio.h>

void		print_usage()
{
  printf("Usage: ./serveur [port]\nWith port between [1024;65000]\n");
}
