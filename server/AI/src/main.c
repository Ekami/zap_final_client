/*
 ** test.c for zappy in /home/godard_b/workspace/zappy-2016-letour_t/server/src/AI
 **
 ** Made by godard_b
 ** Login   <godard_b@epitech.net>
 **
 ** Started on  Fri Jun  7 21:56:21 2013 godard_b
** Last update Tue Jun 11 17:55:21 2013 tuatini godard
 */

#include <stdio.h>
#include <stdlib.h>
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "luaconf.h"

int		main(int ac, char **av)
{
  int		status;
  int		ret;
  lua_State	*L;

  L = luaL_newstate();
  luaL_openlibs(L);
  status = luaL_loadfile(L, "src/core.lua");
  if (status) {
    fprintf(stderr, "Couldn't load file: %s\n", lua_tostring(L, -1));
    exit(1);
  }
  lua_pcall (L, 0, 0, 0);
  lua_getglobal(L, "my_fct");
  lua_pushnumber(L, 10);
  lua_pushnumber(L, 12);
  if (lua_pcall(L, 2, 1, 0) != 0)
    error(L, "Error while running function my_fct: %s", lua_tostring(L, -1));

  if (!lua_isnumber(L, -1))
    error(L, "function my_fct must return a number");
  ret = lua_tonumber(L, -1);
  lua_pop(L, 1);
  printf("The lua function returned %d\n", ret);
  lua_close(L);

  return 0;
}
