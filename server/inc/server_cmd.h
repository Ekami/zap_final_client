/*
** server_cmd.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 13:06:30 2013 julien lebot
** Last update Sun Apr 28 17:22:55 2013 julien lebot
*/

#ifndef		SERVER_CMD_H_
# define	SERVER_CMD_H_

# include "connect.h"
# include "server_data_context.h"

typedef	void	(*t_server_cmd_fn)(int, char **,
				   t_client *,
				   t_server_data *);

typedef	struct		s_server_cmd
{
  const	char		*cmd;
  t_server_cmd_fn	fn;
}			t_server_cmd;

#endif /* !SERVER_CMD_H_ */
