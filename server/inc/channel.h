/*
** channel.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 15:56:09 2013 julien lebot
** Last update Sat Apr 27 18:29:25 2013 julien lebot
*/

#ifndef		CHANNEL_H_
# define	CHANNEL_H_

# include "strmap.h"

typedef	struct	s_strtree	t_strtree;

typedef	struct	s_channel
{
  char		*name;
  t_strtree	*users;
}		t_channel;

#endif /* !CHANNEL_H_ */
