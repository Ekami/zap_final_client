/*
** send_helpers.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 18:17:42 2013 julien lebot
** Last update Sun Apr 28 17:44:03 2013 julien lebot
*/

#ifndef		SEND_HELPERS_H_
# define	SEND_HELPERS_H_

# include "server_data_context.h"

void	send_message(char *msg,
		     int code,
		     t_client *client,
		     t_server_data *ctx);

int	write_user_response(char *msg,
			    int size,
			    t_client *client);

void	send_welcome_message(t_client *c,
			     t_server_data * ctx);

void	send_host_message(t_client *c,
			  t_server_data *ctx);

void	send_server_created(t_client *c,
			    t_server_data *ctx);

void	send_server_info(t_client *c,
			 t_server_data *ctx);

void	send_notice(t_client *c,
		    t_server_data *ctx);

#endif /* !SEND_HELPERS_H_ */
