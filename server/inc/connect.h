/*
** connect.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Wed Apr 24 01:41:11 2013 julien lebot
** Last update Sun Apr 28 18:42:54 2013 julien lebot
*/

#ifndef		CONNECT_H_
# define	CONNECT_H_

# include "connection.h"
# include "btree.h"

typedef	struct	s_endpoint
{
  const char	*address;
  int		port;
}		t_endpoint;

t_connection	*client_connect(t_endpoint endpoint,
				t_callbacks cbs,
				void *ctx);

void		new_client(t_btree **work_tree,
			   int fd,
			   void *data);

#endif	/* !CONNECT_H_ */
