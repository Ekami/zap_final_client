/*
** server_data_context.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Apr 25 20:09:48 2013 julien lebot
** Last update Sun Apr 28 19:21:55 2013 julien lebot
*/

#ifndef		SERVER_DATA_CONTEXT_H_
# define	SERVER_DATA_CONTEXT_H_

# include <limits.h>
# include "btree.h"
# include "strmap.h"
# include "connection.h"
# include "double_list.h"

typedef struct	s_strtree	t_strtree;

# define	RPL_WELCOME	1
# define	RPL_YOURHOST	2
# define	RPL_CREATED	3
# define	RPL_MYINFO	4
# define	ASCTIME_SIZE	26
# define	IPADDR_SIZE	17

/*
** work_tree : contains a list of active connections
**	       sorted by file descriptor (highest to the left)
** channels  : contains a list of channels
**	       sorted by name
*/
typedef struct	s_server_data
{
  t_btree	*work_tree;
  t_strtree	*channels;
  char		server_name[HOST_NAME_MAX];
  int		server_name_size;
  int		server_fd;
  int		server_port;
  char		ipaddress[IPADDR_SIZE];
  int		closing;
  char		created[ASCTIME_SIZE];
}		t_server_data;

typedef	struct	s_client
{
  t_connection	connect;
  t_double_list	*channels;
  int		client_state;
  char		*nickname;
  char		*realname;
  char		*hostname;
  char		ipaddress[IPADDR_SIZE];
}		t_client;

typedef	struct	s_buffer
{
  char		*data;
  int		idx;
  int		cap;
}		t_buffer;

typedef	struct	s_message
{
  t_buffer	*buf;
  char		*recipient;
}		t_message;

typedef	struct	s_client_state_fn
{
  int		value;
  void		(*fn)(t_client *, t_server_data *);
}		t_client_state_fn;

#endif /* !SERVER_DATA_CONTEXT_H_ */
