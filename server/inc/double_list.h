/*
** double_list.h for  in /home/lebot_j//afs/myselect-2016ed-2015s-2017si-lebot_j
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Nov 25 22:38:18 2012 julien lebot
** Last update Sat Apr 27 16:26:28 2013 julien lebot
*/

#ifndef		DOUBLE_LIST_H_
# define	DOUBLE_LIST_H_

typedef	struct		s_double_list
{
  void			*data;
  struct s_double_list	*next;
  struct s_double_list	*prev;
}			t_double_list;

typedef	int	(*t_dl_cmp_fn)(void *, void *);

t_double_list	*dl_insert(t_double_list **list, void *item);
t_double_list	*dl_insert_after(t_double_list *after, void *item);
void		dl_erase(t_double_list **list, void *item, t_dl_cmp_fn);
void		dl_free(t_double_list **list, void (*free_fn)(void *));
t_double_list	*dl_last(t_double_list *list);

#endif /* !DOUBLE_LIST_H_ */
