/*
** circular_buffer.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 11:50:40 2013 julien lebot
** Last update Sun Apr 28 05:09:04 2013 julien lebot
*/

#ifndef		CIRCULAR_BUFFER_H_
# define	CIRCULAR_BUFFER_H_

typedef	struct	s_cb_element
{
  char		*data;
  int		size;
}		t_cb_element;

/*
** Circular buffer object
** num: number of buffers
** size: size of buffer
** start: index of oldest element
** end: index at which to write new element
** elems: vector of elements
*/
typedef struct	s_circular_buffer
{
  int			num;
  int			size;
  int			start;
  int			end;
  t_cb_element		*elems;
}			t_circular_buffer;

int	cb_init(t_circular_buffer *cb, int size, int num_elems);

void	cb_free(t_circular_buffer *cb);

int	cb_is_full(t_circular_buffer *cb);

int	cb_is_empty(t_circular_buffer *cb);

void	cb_set_empty(t_circular_buffer* cb);

int	cb_size(t_circular_buffer *cb);

/*
** Write an element, overwriting oldest element if buffer is full.
** App can choose to avoid the overwrite by checking cb_is_full().
*/
int	cb_write(t_circular_buffer *cb, char *elem, int size);

/*
** Read oldest element. App must ensure !cb_is_empty() first.
*/
int	cb_read(t_circular_buffer *cb, char *elem, int size);

#endif	/* !CIRCULAR_BUFFER_H_ */
