/*
** connection.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr 23 19:45:53 2013 julien lebot
** Last update Sun Apr 28 20:58:30 2013 julien lebot
*/

#ifndef		CONNECTION_H_
# define	CONNECTION_H_

# include "circular_buffer.h"

/*
** As per the RFC specification
** includes \r\n, so real message
** length cannot exceed 510 bytes.
*/
# define	BUFSIZE	512

# if BUFSIZE < 512
#  error "Buffer size too small"
# endif

/*
** 32 * 512 bytes ~ 16.34KB
*/
# define	BUF_NUM	32

# if BUF_NUM < 2
#  error "Buffer number too small"
# endif

struct	s_connection;
struct	sockaddr_storage;

typedef	void (*callback_fn)(struct s_connection *, void *);

typedef	struct	s_callbacks
{
  callback_fn	read_cb;
  callback_fn	write_cb;
  callback_fn	disconnect_cb;
}		t_callbacks;

typedef	struct		s_connection
{
  int			fd;
  int			closing;
  t_circular_buffer	read_buffer;
  t_circular_buffer	write_buffer;
  t_callbacks		callbacks;
  void			*ctx;
}			t_connection;

/*
** Allocates memory and perform initialization
** for the connection.
*/
int	init_connection(t_connection *connect,
			int fd,
			t_callbacks cbs,
			void *ctx);

/*
** Frees the memory allocated by init_connection
** The connection is un-usable after this call.
*/
void	free_connection(t_connection *connect);

/*
** void * used because -fpermissive of g++
*/
int	compare_clients(void *a, void *b);

/*
** void * used because -fpermissive of g++
*/
int	compare_connections(void *a, void *b);

int	parse_ipaddress(struct sockaddr_storage *addr,
			char *ipaddress,
			int size);

#endif	/* !CONNECTION_H_ */
