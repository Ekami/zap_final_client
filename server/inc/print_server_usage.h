/*
** print_server_usage.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 16:44:42 2013 julien lebot
** Last update Sun Apr 28 16:45:02 2013 julien lebot
*/

#ifndef		PRINT_SERVER_USAGE_H_
# define	PRINT_SERVER_USAGE_H_

void		print_usage();

#endif /* !PRINT_SERVER_USAGE_H */
