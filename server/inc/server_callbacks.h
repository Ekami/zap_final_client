/*
** server_callbacks.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 03:49:35 2013 julien lebot
** Last update Sun Apr 28 03:50:21 2013 julien lebot
*/

#ifndef		SERVER_CALLBACKS_H_
# define	SERVER_CALLBACKS_H_

# include "server_data_context.h"

void		read_cb(t_client *c, t_server_data *ctx);
void		write_cb(t_client *c, t_server_data * ctx);
void		disconnect_cb(t_client *c, t_server_data *ctx);

#endif /* !SERVER_CALLBACKS_H_ */
