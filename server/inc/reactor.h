/*
** reactor.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Wed Apr 24 15:25:18 2013 julien lebot
** Last update Fri Apr 26 23:35:43 2013 julien lebot
*/

#ifndef		REACTOR_H_
# define	REACTOR_H_

# include <unistd.h>
# include "btree.h"
# include "connection.h"

typedef	enum	e_set_name
{
  e_read_set = 0,
  e_write_set = 1
}		t_set_name;

void	add_item_to_set(t_connection *item, fd_set *sets);

int	configure_sets(t_btree *work_tree,
		       fd_set *sets);

void	do_work(t_btree **work_tree,
		int server_fd,
		void (*new_client_fn)(t_btree **, int, void *),
		void *ctx);

#endif /* !REACTOR_H_ */
