/*
** ftp_socks.h for  in /home/lebot_j//Dev/myftp-2015s-2016-2017si-lebot_j
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Tue Apr  9 04:15:45 2013 julien lebot
** Last update Sun Apr 28 20:47:42 2013 julien lebot
*/

#ifndef		FTP_SOCKS_H_
# define	FTP_SOCKS_H_

int	new_sock_client(char const *ip, int port);
int	new_sock_server(int port,
			char *ipaddress,
			int size);

#endif /* !FTP_SOCKS_H_ */
