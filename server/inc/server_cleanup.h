/*
** server_cleanup.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sun Apr 28 15:06:21 2013 julien lebot
** Last update Sun Apr 28 15:07:53 2013 julien lebot
*/

#ifndef		SERVER_CLEANUP_H_
# define	SERVER_CLEANUP_H_

# include "server_data_context.h"

void		do_cleanup(t_server_data *data);

#endif /* !SERVER_CLEANUP_H_ */
