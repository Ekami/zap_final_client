/*
** strmap.h for  in /home/lebot_j//Dev/my_ftp
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Mon Apr  8 08:37:19 2013 julien lebot
** Last update Sun Apr 28 17:23:08 2013 julien lebot
*/

#ifndef	STRMAP_H_
# define STRMAP_H_

/*
** Opaque data type mapping a string
** to some user data a.k.a std::map<std::string, void*>
*/
struct s_strtree;

typedef	struct	s_str_key_value
{
  char const	*key;
  void		*value;
}		t_str_key_value;

int	strmap_cmp(t_str_key_value *kvp1, t_str_key_value *kvp2);

int	strmap_insert(struct s_strtree **tree,
		      char const *key,
		      void *value);

void	*strmap_get(struct s_strtree *tree,
		    char const *key);

void	strmap_remove(struct s_strtree **tree,
		      char const *key);

void	strmap_destroy(struct s_strtree **tree);

typedef	void	(*t_strtree_apply_fn)(char const *key,
				      void *data,
				      void *ctx);

void	strmap_apply(struct s_strtree *tree,
		     t_strtree_apply_fn fn,
		     void *ctx);

#endif /* STRMAP_H_ */
