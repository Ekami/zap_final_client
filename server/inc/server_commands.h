/*
** server_commands.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Sat Apr 27 18:20:14 2013 julien lebot
** Last update Sun Apr 28 05:06:13 2013 julien lebot
*/

#ifndef		SERVER_COMMANDS_H_
# define	SERVER_COMMANDS_H_

# include "server_data_context.h"

void		server_cmd_user(int argc,
				char **argv,
				t_client *c,
				t_server_data *data);

void		server_cmd_nick(int argc,
				char **argv,
				t_client *c,
				t_server_data *data);

void		server_cmd_join(int argc,
				char **argv,
				t_client *client,
				t_server_data *data);

void		server_cmd_list(int argc,
				char **argv,
				t_client *client,
				t_server_data *data);

void		server_cmd_names(int argc,
				 char **argv,
				 t_client *client,
				 t_server_data *data);

void		server_cmd_part(int argc,
				char **argv,
				t_client *client,
				t_server_data *data);

void		server_cmd_privmsg(int argc,
				   char **argv,
				   t_client *client,
				   t_server_data *data);

void		leave_channel(char *chan,
			      t_client *client,
			      t_server_data *data);

void		send_message_to_user(t_buffer *message,
				     char *user,
				     t_client *client,
				     t_server_data *data);

void		send_message_to_channel(t_buffer *message,
					char *chan,
					t_client *client,
					t_server_data *data);

void		server_cmd_quit(int argc,
				char **argv,
				t_client *c,
				t_server_data *data);

#endif /* !SERVER_COMMANDS_H_ */
