/*
** btree.h for  in /home/lebot_j/afs/rendu_jour14
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Thu Oct 25 09:10:04 2012 julien lebot
** Last update Sun Apr 28 14:41:35 2013 julien lebot
*/

#ifndef		BTREE_H_
# define	BTREE_H_

typedef struct		s_btree
{
  struct s_btree	*left;
  struct s_btree	*right;
  void			*data;
}			t_btree;

# ifndef	MAX
#  define	MAX(a, b)	((a) > (b) ? (a) : (b))
# endif
# ifndef	NULL
#  define	NULL		((void *)0)
# endif

void		btree_apply_by_level(t_btree *root,
				     void (*applyf)(void *item,
						    int current_level,
						    int is_first_elem));

typedef		void	(*t_btree_apply_fn)(void *data, void *ctx);
typedef		int	(*t_btree_cmp_fn)(void *, void *);

void		btree_apply_infix(t_btree *root,
				  t_btree_apply_fn fn,
				  void *ctx);

void		btree_apply_prefix(t_btree *root,
				   t_btree_apply_fn fn,
				   void *ctx);

void		btree_apply_suffix(t_btree *root,
				   t_btree_apply_fn fn,
				   void *ctx);

t_btree		*btree_create_node(void *data);

int		btree_insert_data(t_btree **root,
				  void *data,
				  t_btree_cmp_fn fn);

int		btree_insert_branch(t_btree *root,
				    t_btree *branch,
				    int (*cmpf)(void *, void *));

void		btree_remove_item(t_btree **root,
				  void *data_ref,
				  t_btree_cmp_fn fn,
				  void (*freefn)(void *));

void		*btree_search_item(t_btree *root,
				   void *data_ref,
				   t_btree_cmp_fn fn);

void		btree_free(t_btree *root, void (*freefn)(void *));

#endif /* !BTREE_H_ */
