/*
** string_utils.h for  in /home/lebot_j//Tek2/myirc-2016-nicopo_v
** 
** Made by julien lebot
** Login   <lebot_j@epitech.net>
** 
** Started on  Fri Apr 26 00:10:49 2013 julien lebot
** Last update Sat Apr 27 12:51:54 2013 julien lebot
*/

#ifndef		STRING_UTILS_H_
# define	STRING_UTILS_H_

int		count_word(char *str);
int		my_str_to_wordtab(char *str, char ***tab);
void		free_wordtab(char **str);
const char	*my_str_find_first_of(const char *str, char what);
int		my_str_find_last(const char *str, char c, int start);
const char	*my_str_find_last_of(const char *str, char c);
const char	*my_str_find_next_not_of(const char *str, char c);

#endif /* !STRING_UTILS_H_ */
