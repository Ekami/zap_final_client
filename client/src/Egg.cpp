/*
 * Egg.cpp
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#include "Egg.hh"

Egg::Egg(const int x, const int y, const Color color)
{
	this->x = x;
	this->y = y;
	this->color = color;
}
