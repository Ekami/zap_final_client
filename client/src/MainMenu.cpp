/* 
 * File:   MainMenu.cpp
 * Author: gauthi_d
 * 
 * Created on June 10, 2013, 12:02 AM
 */

#include "Button.hh"
#include "MainMenu.hh"

MainMenu::MainMenu()
{
  InitMenu();
}

MainMenu::~MainMenu()
{
  delete _playButton;
}

sf::Sprite MainMenu::GetMainSprite() const
{
  return _mainSprite;
}

void MainMenu::SetMainSprite(sf::Sprite _mainSprite)
{
  this->_mainSprite = _mainSprite;
}

sf::Texture MainMenu::GetMainTexture() const
{
  return _mainTexture;
}

void MainMenu::SetMainTexture(sf::Texture _mainTexture)
{
  this->_mainTexture = _mainTexture;
}

void MainMenu::InitMenu()
{
  this->_mainTexture.loadFromFile("ressources/menu.png");
  this->_playButton = new Button("button_zappy2.png", "playtime.ttf", "Start", *this);
  this->_playButton->SetPosition(270, 360);
  this->_aboutButton = new Button("button_zappy2.png", "playtime.ttf", "About", *this);
  this->_aboutButton->SetPosition(270, 425);
  this->_exitButton = new Button("button_zappy2.png", "playtime.ttf", "Exit", *this);
  this->_exitButton->SetPosition(270, 490);
  this->_mainSprite.setTexture(_mainTexture);
  SetIsVisible(true);
}

bool MainMenu::GetIsIsVisible() const
{
  return _isVisible;
}

void MainMenu::SetIsVisible(bool _isVisible)
{
  this->_isVisible = _isVisible;
}

Button* MainMenu::GetPlayButton() const
{
  return _playButton;
}

Button* MainMenu::GetAboutButton() const
{
  return _aboutButton;
}

Button* MainMenu::GetExitButton() const
{
  return _exitButton;
}

