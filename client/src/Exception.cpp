#include	"Exception.hh"

GlobExcep::GlobExcep(std::string error) : _errormsg(error)
{}

GlobExcep::~GlobExcep() throw()
{}

const char	*GlobExcep::what() const throw()
{
  return (_errormsg.c_str());
}
