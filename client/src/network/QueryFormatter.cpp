/*
 * ProtocolFormatter.cpp
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#include "QueryFormatter.hh"

namespace Zappy {

std::string	QueryFormatter::mapSizeQuery()
{
	return "msz\n";
}

std::string	QueryFormatter::caseContentQuery(const int X, const int Y)
{
	std::stringstream	str;

	str <<  "bct " << X << " " << Y << "\n";
	return str.str();
}

std::string	QueryFormatter::mapContentQuery()
{
	return "mct\n";
}

std::string	QueryFormatter::teamNamesQuery()
{
	return "tna\n";
}

std::string	QueryFormatter::playerPositionQuery(const std::string &playerName)
{
	std::stringstream	str;

	str << "ppo " << playerName << "\n";
	return str.str();
}

std::string	QueryFormatter::playerLevelQuery(const std::string &playerName)
{
	std::stringstream	str;

	str << "plv " << playerName << "\n";
	return str.str();
}

std::string	QueryFormatter::playerBagQuery(const std::string &playerName)
{
	std::stringstream	str;

	str << "pin " << playerName << "\n";
	return str.str();
}

std::string QueryFormatter::timeUnitQuery()
{
	return "sgt\n";
}

std::string	QueryFormatter::timeUnitModificationQuery(const int timeUnit)
{
	std::stringstream	str;

	str << "sst " << timeUnit << "\n";
	return str.str();
}

} /* namespace Zappy */
