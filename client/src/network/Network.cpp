/*
 * Network.cpp
 *
 *  Created on: Jun 14, 2013
 *      Author: godard_b
 */

#include "Network.hh"

Network::Network(const std::string &ip, const int port, const IUICallbacks &callbacks) {
	struct sockaddr_in    server_addr;

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		throw NetworkException::InvalidParameters();
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	if (inet_pton(AF_INET, ip.c_str(), &server_addr.sin_addr) != 1)
		throw NetworkException::InvalidParameters();
	if (connect(server_fd, (struct sockaddr *)&server_addr,
			sizeof(server_addr)) == -1)
		throw NetworkException::ConnectionFailed();
}

Network::~Network() {

}


