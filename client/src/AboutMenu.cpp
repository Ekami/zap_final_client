/* 
 * File:   AboutMenu.cpp
 * Author: gauthi_d
 * 
 * Created on June 10, 2013, 12:01 AM
 */

#include "AboutMenu.hh"
#include "Button.hh"

AboutMenu::AboutMenu()
{
  InitAboutMenu();
}

void AboutMenu::InitAboutMenu()
{
  this->_aboutTexture.loadFromFile("ressources/about.png");
  this->_aboutSprite.setTexture(_aboutTexture);
  this->_backButton = new Button("button_zappy2.png", "playtime.ttf", "Back", *this);
  this->_backButton->SetPosition(270, 490);
}

AboutMenu::~AboutMenu()
{
}

sf::Sprite AboutMenu::GetAboutSprite() const
{
  return _aboutSprite;
}

void AboutMenu::SetAboutSprite(sf::Sprite _aboutSprite)
{
  this->_aboutSprite = _aboutSprite;
}

sf::Texture AboutMenu::GetAboutTexture() const
{
  return _aboutTexture;
}

void AboutMenu::SetAboutTexture(sf::Texture _aboutTexture)
{
  this->_aboutTexture = _aboutTexture;
}

bool AboutMenu::GetIsIsVisible() const
{
  return _isVisible;
}

void AboutMenu::SetIsVisible(bool _isVisible)
{
  this->_isVisible = _isVisible;
}

Button* AboutMenu::GetBackButton() const
{
  return _backButton;
}
