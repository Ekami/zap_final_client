/* 
 * File:   MainWindow.cpp
 * Author: gauthi_d
 * 
 * Created on 6 juin 2013, 10:48
 */

#include "MainWindow.hh"

MainWindow::MainWindow()
: sf::RenderWindow()
{
  InitWindow();
}

MainWindow::MainWindow(const MainWindow& orig)
: sf::RenderWindow()
{
}

MainWindow::~MainWindow()
{
}

sf::RenderWindow* MainWindow::GetMainWindow() const
{
  return _mainWindow;
}

void MainWindow::SetMainWindow(sf::RenderWindow *_mainWindow)
{
  this->_mainWindow = _mainWindow;
}

void MainWindow::InitWindow()
{
  _mainWindow = new sf::RenderWindow;
  _mainWindow->create(sf::VideoMode(635, 805), "Zappy", sf::Style::Titlebar | sf::Style::Close);
  _mainWindow->clear(sf::Color::Blue);
}