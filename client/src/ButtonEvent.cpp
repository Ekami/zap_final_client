/* 
 * File:   ButtonEvent.cpp
 * Author: gauthi_d
 * 
 * Created on June 10, 2013, 7:07 PM
 */

#include <iostream>
#include "ButtonEvent.hh"

ButtonEvent::ButtonEvent(int rt, int lt, int rb, int lb)
: _rightTopCorner(rt)
, _leftTopCorner(lt)
, _rightBottomCorner(rb)
, _leftBottomCorner(lb)
{
}

ButtonEvent::ButtonEvent(const ButtonEvent& other)
{
}

ButtonEvent::ButtonEvent()
{
}

ButtonEvent::~ButtonEvent()
{
}

int ButtonEvent::getLeftBottomCorner() const
{
  return _leftBottomCorner;
}

void ButtonEvent::setLeftBottomCorner(int _leftBottomCorner)
{
  this->_leftBottomCorner = _leftBottomCorner;
}

int ButtonEvent::getLeftTopCorner() const
{
  return _leftTopCorner;
}

void ButtonEvent::setLeftTopCorner(int _leftTopCorner)
{
  this->_leftTopCorner = _leftTopCorner;
}

int ButtonEvent::getRightBottomCorner() const
{
  return _rightBottomCorner;
}

void ButtonEvent::setRightBottomCorner(int _rightBottomCorner)
{
  this->_rightBottomCorner = _rightBottomCorner;
}

int ButtonEvent::getRightTopCorner() const
{
  return _rightTopCorner;
}

void ButtonEvent::setRightTopCorner(int _rightTopCorner)
{
  this->_rightTopCorner = _rightTopCorner;
}

bool ButtonEvent::checkRightTopCorner(int mouse)
{
  if (mouse < this->_rightTopCorner)
    return true;
  return false;
}

bool ButtonEvent::checkLeftTopCorner(int mouse)
{
  if (mouse > this->_leftTopCorner)
    return true;
  return false;
}

bool ButtonEvent::checkRightBottomCorner(int mouse)
{
  if (mouse < this->_rightBottomCorner)
    return true;
  return false;
}

bool ButtonEvent::checkLeftBottomCorner(int mouse)
{
  if (mouse > this->_leftBottomCorner)
    return true;
  return false;
}

ButtonEvent const & ButtonEvent::operator=(ButtonEvent const &other)
{
  this->_leftBottomCorner = other.getLeftBottomCorner();
  this->_leftTopCorner = other.getLeftTopCorner();
  //  this->_ptrWindow = other.;
  this->_rightBottomCorner = other.getRightBottomCorner();
  this->_rightTopCorner = other.getRightTopCorner();
  return (*this);
}

bool ButtonEvent::checkAll(int mouseX, int mouseY)
{
  if (this->checkLeftBottomCorner(mouseY)
          && this->checkLeftTopCorner(mouseX)
          && this->checkRightBottomCorner(mouseY)
          && this->checkRightTopCorner(mouseX))
    return true;
  return false;
}