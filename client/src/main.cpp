/* 
 * File:   main.cpp
 * Author: gauthi_d
 *
 * Created on 6 juin 2013, 10:28
 */

#include <iostream>
#include "Button.hh"
#include "Exception.hh"
#include "MainWindow.hh"
#include "AboutMenu.hh"
#include "MainMenu.hh"

int main(int argc, char** argv)
{
  try
    {
      MainWindow _mainWindow;
      AboutMenu _aboutMenu;
      MainMenu _mainMenu;

      _mainWindow.GetMainWindow()->draw(_mainMenu.GetMainSprite());
      _mainWindow.GetMainWindow()->draw(_mainMenu.GetPlayButton()->GetSprite());
      _mainWindow.GetMainWindow()->draw(_mainMenu.GetAboutButton()->GetSprite());
      _mainWindow.GetMainWindow()->draw(_mainMenu.GetExitButton()->GetSprite());
      _mainWindow.GetMainWindow()->draw(_mainMenu.GetPlayButton()->GetText());
      _mainWindow.GetMainWindow()->draw(_mainMenu.GetAboutButton()->GetText());
      _mainWindow.GetMainWindow()->draw(_mainMenu.GetExitButton()->GetText());
      _mainWindow.GetMainWindow()->display();
      while (_mainWindow.GetMainWindow()->isOpen())
        {
          sf::Event event;
          while (_mainWindow.GetMainWindow()->pollEvent(event))
            {
              if (event.type == sf::Event::Closed)
                _mainWindow.GetMainWindow()->close();
              if (event.type == sf::Event::MouseButtonPressed)
                {
                  if (event.mouseButton.button == sf::Mouse::Left
                          && _mainMenu.GetAboutButton()->GetEvent().checkAll(sf::Mouse::getPosition(*_mainWindow.GetMainWindow()).x, sf::Mouse::getPosition(*_mainWindow.GetMainWindow()).y)
                          && _mainMenu.GetIsIsVisible())
                    {
                      _mainWindow.GetMainWindow()->clear(sf::Color::Blue);
                      _mainWindow.GetMainWindow()->draw(_aboutMenu.GetAboutSprite());
                      _mainWindow.GetMainWindow()->draw(_aboutMenu.GetBackButton()->GetSprite());
                      _mainWindow.GetMainWindow()->draw(_aboutMenu.GetBackButton()->GetText());
                      _mainWindow.GetMainWindow()->display();
                      _aboutMenu.SetIsVisible(true);
                      _mainMenu.SetIsVisible(false);
                    }
                  if (event.mouseButton.button == sf::Mouse::Left
                          && _aboutMenu.GetBackButton()->GetEvent().checkAll(sf::Mouse::getPosition(*_mainWindow.GetMainWindow()).x, sf::Mouse::getPosition(*_mainWindow.GetMainWindow()).y)
                          && !_mainMenu.GetIsIsVisible())
                    {
                      _mainWindow.GetMainWindow()->clear(sf::Color::Blue);
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetMainSprite());
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetPlayButton()->GetSprite());
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetPlayButton()->GetText());
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetAboutButton()->GetSprite());
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetAboutButton()->GetText());
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetExitButton()->GetSprite());
                      _mainWindow.GetMainWindow()->draw(_mainMenu.GetExitButton()->GetText());
                      _mainWindow.GetMainWindow()->display();
                      _aboutMenu.SetIsVisible(false);
                      _mainMenu.SetIsVisible(true);
                      break;
                    }
                  if (event.mouseButton.button == sf::Mouse::Left
                          && _mainMenu.GetExitButton()->GetEvent().checkAll(sf::Mouse::getPosition(*_mainWindow.GetMainWindow()).x, sf::Mouse::getPosition(*_mainWindow.GetMainWindow()).y)
                          && !_aboutMenu.GetIsIsVisible()
                          && _mainMenu.GetIsIsVisible())
                    {
                      _mainWindow.GetMainWindow()->close();
                    }
                }
            }
        }
    }
  catch (const GlobExcep &e)
    {
      std::cerr << "Error : " << e.what() << std::endl;
    }

  return 0;
}