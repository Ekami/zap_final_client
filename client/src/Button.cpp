/* 
 * File:   Button.cpp
 * Author: gauthi_d
 * 
 * Created on 9 juin 2013, 13:23
 */

#include "Exception.hh"
#include "Button.hh"

Button::Button(std::string strBG, std::string strFont, std::string strText, sf::RenderWindow &ptr)
: _strBG(strBG)
, _strFont(strFont)
, _strText(strText)
, _ptrWindow(ptr)
{
    initButton();
}

Button::Button(const Button & other)
: _ptrWindow(other.GetPtrWindow())
{
}


Button::~Button()
{
}

sf::RenderWindow &Button::GetPtrWindow() const
{
    return _ptrWindow;
}

sf::Texture Button::GetBackground() const
{
    return _background;
}

sf::Font Button::GetFont() const
{
    return _font;
}

sf::Text Button::GetText() const
{
    return _text;
}

std::string Button::GetStrBG() const
{
    return _strBG;
}

std::string Button::GetStrFont() const
{
    return _strFont;
}

std::string Button::GetStrText() const
{
    return _strText;
}

sf::Sprite Button::GetSprite() const
{
    return _sprite;
}

ButtonEvent &Button::GetEvent()
{
    return _event;
}

void Button::loadBG()
{
    if (!this->_background.loadFromFile("ressources/" + this->_strBG))
      {
        throw GlobExcep("Load Image Background");
      }
}

void Button::loadFont()
{
    if (!this->_font.loadFromFile("ressources/" + this->_strFont))
      {
        throw GlobExcep("Load Font");
      }
}

void Button::SetPosition(float x, float y)
{
    sf::FloatRect rect = _text.getGlobalBounds();
    rect.left = ((_background.getSize().x / 2)- (rect.width / 2));
    rect.top = ((_background.getSize().y / 2) - (rect.height / 2));
    this->_text.setPosition(rect.left + x, rect.top + y - 8);
    this->_sprite.setPosition(x, y);
    this->_event.setLeftTopCorner(_sprite.getPosition().x);
    this->_event.setRightTopCorner(_sprite.getPosition().x + _sprite.getGlobalBounds().width);
    this->_event.setLeftBottomCorner(_sprite.getPosition().y);
    this->_event.setRightBottomCorner(_sprite.getPosition().y + _sprite.getGlobalBounds().height);
}

void Button::initButton()
{
    this->loadBG();
    this->loadFont();
    this->_sprite.setTexture(this->_background);
    this->_text.setFont(this->_font);
    this->_text.setString(this->_strText);
    this->_text.setColor(sf::Color::Yellow);
    this->_text.setCharacterSize(30);
}

void Button::drawButton()
{
}