/*
 * Player.cpp
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#include "Trantorian.hh"

static int	playerCount = 0;

Trantorian::Trantorian(const std::string &teamName, const int posX, const int posY)
{
	this->teamName = teamName;
	this->id = playerCount++;
	this->x = posX;
	this->y = posY;
}

std::string	Trantorian::getTeamName() const
{
	return teamName;
}

int	Trantorian::getId() const
{
	return id;
}

void	Trantorian::setXPos(const int posX)
{
	this->x = posX;
}

void	Trantorian::setYPos(const int posY)
{
	this->y = posY;
}

int		Trantorian::getXPos() const
{
	return x;
}

int		Trantorian::getYPos() const
{
	return y;
}
