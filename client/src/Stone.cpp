/*
 * Stone.cpp
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#include "Stone.hh"

Stone::Stone(const int x, const int y, const StonesType type) {
	this->x = x;
	this->y = y;
	this->type = type;
}

