/*
 * CallbacksHandler.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef CALLBACKSHANDLER_HH_
#define CALLBACKSHANDLER_HH_
#include <iostream>
#include <string>
#include "IUICallbacks.hh"

class UICallbacksHandler : public IUICallbacks
{
public:
	void	initMap() = 0;
	void	clearMap() = 0;
	void	drawTrantorian(const Trantorian &trant) = 0;
	void	drawEgg(const Egg &egg) = 0;
	void	drawStone(const Stone &stone) = 0;
	void	moveTrantorian(const Trantorian &trant) = 0;
};

#endif /* CALLBACKSHANDLER_HH_ */
