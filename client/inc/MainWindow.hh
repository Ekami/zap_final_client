/* 
 * File:   ClassTest.hh
 * Author: gauthi_d
 *
 * Created on 6 juin 2013, 10:48
 */

#ifndef MAINWINDOW_HH
#define	MAINWINDOW_HH

#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

class MainWindow : public sf::RenderWindow
{
public:
    MainWindow();
    MainWindow(const MainWindow& orig);
    virtual             ~MainWindow();
    sf::RenderWindow*   GetMainWindow() const;
    void                SetMainWindow(sf::RenderWindow *_mainWindow);
    void                InitWindow();

private:
    sf::RenderWindow *_mainWindow;

};

#endif	/* MAINWINDOW_HH */

