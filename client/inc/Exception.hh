#ifndef		EXCEPTION_HH_
# define	EXCEPTION_HH_

#include	<exception>
#include	<string>
#include	<iostream>

class		GlobExcep : public std::exception
{
protected :
  std::string	_errormsg;
public :
  GlobExcep(std::string error);
  ~GlobExcep() throw();
  const char*	what() const throw();
};

#endif		/* !EXCEPTION_HH_ */
