/* 
 * File:   MainMenu.hh
 * Author: gauthi_d
 *
 * Created on June 10, 2013, 12:02 AM
 */

#ifndef MAINMENU_HH
#define	MAINMENU_HH

#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

#include "Button.hh"

class Button;

class MainMenu : public sf::RenderWindow
{
public:
    MainMenu();
    virtual     ~MainMenu();
    sf::Sprite  GetMainSprite() const;
    void        SetMainSprite(sf::Sprite _aboutSprite);
    sf::Texture GetMainTexture() const;
    void        SetMainTexture(sf::Texture _aboutTexture);
    void        InitMenu();
    bool        GetIsIsVisible() const;
    void        SetIsVisible(bool _isVisible);
    Button*     GetPlayButton() const;
    Button*     GetAboutButton() const;
    Button*     GetExitButton() const;
 private:
    sf::Sprite _mainSprite;
    sf::Texture _mainTexture;
    bool        _isVisible;
    Button      *_playButton;
    Button      *_aboutButton;
    Button      *_exitButton;


};

#endif	/* MAINMENU_HH */

