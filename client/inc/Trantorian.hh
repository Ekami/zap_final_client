/*
 * Player.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef PLAYER_HH_
#define PLAYER_HH_
#include <iostream>
#include <string>

class Trantorian {
private:
	std::string	teamName;
	int			id;
	int			x, y;
public:
	Trantorian(const std::string &teamName, const int posX, const int posY);
	std::string	getTeamName() const;
	int			getId() const;
	void		setXPos(const int posX);
	void		setYPos(const int posY);
	int			getXPos() const;
	int			getYPos() const;
};

#endif /* PLAYER_HH_ */
