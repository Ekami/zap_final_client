/* 
 * File:   ButtonEvent.hh
 * Author: gauthi_d
 *
 * Created on June 10, 2013, 7:07 PM
 */

#ifndef BUTTONEVENT_HH
#define	BUTTONEVENT_HH

#include "MainWindow.hh"

class ButtonEvent
{
public:
    ButtonEvent(int rt, int rb, int lt, int lb);
    ButtonEvent(const ButtonEvent& other);
    ButtonEvent();
    virtual ~ButtonEvent();
    
    int getLeftBottomCorner() const;
    void setLeftBottomCorner(int _leftBottomCorner);
    int getLeftTopCorner() const;
    void setLeftTopCorner(int _leftTopCorner);
    int getRightBottomCorner() const;
    void setRightBottomCorner(int _rightBottomCorner);
    int getRightTopCorner() const;
    void setRightTopCorner(int _rightTopCorner);

    bool checkRightTopCorner(int);
    bool checkLeftTopCorner(int);
    bool checkRightBottomCorner(int);
    bool checkLeftBottomCorner(int);
    bool checkAll(int mouseX, int mouseY);

//    int getWindowX() const;
//    void setWindowX(int _windowX);
//    int getWindowY() const;
//    void setWindowY(int _windowY);
    ButtonEvent const &operator=(ButtonEvent const &other);
private:
    int _rightTopCorner;
    int _leftTopCorner;
    int _rightBottomCorner;
    int _leftBottomCorner;
//    int _windowX;
//    int _windowY;
//    sf::RenderWindow& _ptr;

};

#endif	/* BUTTONEVENT_HH */

