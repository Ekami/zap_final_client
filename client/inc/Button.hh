/* 
 * File:   Button.hh
 * Author: gauthi_d
 *
 * Created on 9 juin 2013, 13:23
 */

#ifndef BUTTON_HH
#define	BUTTON_HH

#include <SFML/Graphics.hpp>
#include <string>
#include "ButtonEvent.hh"

class Button
{
public:
    Button(std::string strBG, std::string strFont, std::string strText, sf::RenderWindow &ptr);
    Button(const Button & other);
    Button();
    virtual ~Button();

    sf::Texture GetBackground() const;
    //    sf::Image GetBorder() const;
    sf::Font GetFont() const;
    sf::Text GetText() const;
    std::string GetStrBG() const;
    //    std::string GetStrBorder() const;
    std::string GetStrFont() const;
    std::string GetStrText() const;
    sf::Sprite GetSprite() const;
    sf::RenderWindow &GetPtrWindow() const;
    ButtonEvent &GetEvent();
    
    void        loadBG();
    void        loadFont();
    void        initButton();
    void        drawButton();
    void        SetPosition(float x, float y);

private:
    std::string _strBG;
    //    std::string _strBorder;
    std::string _strFont;
    std::string _strText;
    sf::Texture _background;
    //    sf::Image _border;
    sf::Font _font;
    sf::Text _text;
    sf::Sprite _sprite;
    ButtonEvent _event;
    sf::RenderWindow &_ptrWindow;

};

#endif	/* BUTTON_HH */

