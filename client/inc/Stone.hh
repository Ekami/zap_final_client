/*
 * Stone.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef STONE_HH_
#define STONE_HH_

class Stone {
public:
	enum StonesType
	{
		LINEMATE,
		DERAUMERE,
		SIBUR,
		MENDIANE,
		PHIRAS,
		THYSTAME
	};
	Stone(const int x, const int y, const StonesType type);
private:
	int			x, y;
	StonesType	type;
};

#endif /* STONE_HH_ */
