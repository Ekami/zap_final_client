/* 
 * File:   AboutMenu.hh
 * Author: gauthi_d
 *
 * Created on June 10, 2013, 12:01 AM
 */

#ifndef ABOUTMENU_HH
#define	ABOUTMENU_HH

#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>

#include "Button.hh"

class Button;

class AboutMenu : public sf::RenderWindow
{
public:
    AboutMenu();
    virtual     ~AboutMenu();
    void        InitAboutMenu();

    sf::Sprite  GetAboutSprite() const;
    void        SetAboutSprite(sf::Sprite _aboutSprite);
    sf::Texture GetAboutTexture() const;
    void        SetAboutTexture(sf::Texture _aboutTexture);
    bool        GetIsIsVisible() const;
    void        SetIsVisible(bool _isVisible);
    Button*     GetBackButton() const;

    


private:
    sf::Sprite _aboutSprite;
    sf::Texture _aboutTexture;
    bool        _isVisible;
    Button      *_backButton;
};

#endif	/* ABOUTMENU_HH */

