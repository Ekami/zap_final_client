/*
 * Exceptions.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef EXCEPTIONS_HH_
#define EXCEPTIONS_HH_
#include <iostream>
#include <string>
#include <exception>

namespace NetworkException
{

class NetworkException : public std::exception
{
protected:
	std::string msg;
public:
	virtual ~NetworkException(void) throw () {}
	virtual const char* what(){return msg.c_str();}
};

/*
 * The parameters for socket connection are not correct
 */
class InvalidParameters : public NetworkException
{
public:
	InvalidParameters(const std::string m = "Error: The parameters are invalids!") {msg = m;}
	virtual ~InvalidParameters(void) throw () {}
};

/*
 * The connection to the server failed
 */
class ConnectionFailed : public NetworkException
{
public:
	ConnectionFailed(const std::string m = "Error: Connection to the server failed!") {msg = m;}
	virtual ~ConnectionFailed(void) throw () {}
};

} // namespace NetworkException


#endif /* EXCEPTIONS_HH_ */
