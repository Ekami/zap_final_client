/*
 * ProtocolFormatter.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef PROTOCOLFORMATTER_HH_
#define PROTOCOLFORMATTER_HH_
#include <iostream>
#include <string>
#include <sstream>

namespace Zappy {

class QueryFormatter {
public:
	/*
	 * Taille de la carte
	 */
	static std::string	mapSizeQuery();

	/*
	 * Contenu d’une case de la carte
	 */
	static std::string	caseContentQuery(const int X, const int Y);

	/*
	 * Contenu de la carte (toutes les cases)
	 */
	static std::string	mapContentQuery();

	/*
	 * Nom des équipes.
	 */
	static std::string	teamNamesQuery();

	/*
	 * Position d’un joueur.
	 */
	static std::string	playerPositionQuery(const std::string &playerName);

	/*
	 * Niveau d’un joueur.
	 */
	static std::string	playerLevelQuery(const std::string &playerName);

	/*
	 * Inventaire d’un joueur.
	 */
	static std::string	playerBagQuery(const std::string &playerName);

	/*
	 * Demande de l’unité de temps courante sur le serveur.
	 */
	static std::string timeUnitQuery();

	/*
	 * Modification de l’unité de temps sur le serveur.
	 */
	static std::string	timeUnitModificationQuery(const int timeUnit);
};

} /* namespace Zappy */
#endif /* PROTOCOLFORMATTER_HH_ */
