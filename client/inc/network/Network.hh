/*
 * Network.hh
 *
 *  Created on: Jun 14, 2013
 *      Author: godard_b
 */

#ifndef NETWORK_HH_
#define NETWORK_HH_
#include <iostream>
#include <string>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstring>
#include "IUICallbacks.hh"
#include "Exceptions.hh"

class Network {
private:
	int		server_fd;
public:
	Network(const std::string &ip, const int port, const IUICallbacks &callbacks);
	void	startListening();
	virtual ~Network();
};

#endif /* NETWORK_HH_ */
