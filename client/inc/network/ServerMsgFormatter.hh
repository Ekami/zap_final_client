/*
 * ServerMsgFormatter.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef SERVERMSGFORMATTER_HH_
#define SERVERMSGFORMATTER_HH_
#include <iostream>
#include <string>

namespace Zappy {

class ServerMsgFormatter {
public:
	enum EventType
	{
	/*
	 * Taille de la carte.
	 */
		MAP_SIZE,
	/*
	 * Contenu d’une case de la carte.
	 */
		MAP_BLOCK_CONTENT,
	/*
	 * Contenu de la carte (toutes les cases).
	 */
		MAP_ALL_CONTENT,
	/*
	 * Nom des équipes.
	 */
		TEAMS_NAMES,
	/*
	 * Connexion d’un nouveau joueur.
	 */
		NEW_PLAYER_CONNECTION,
	/*
	 * Position d’un joueur.
	 */
		PLAYER_POSITION,
	/*
	 * Niveau d’un joueur.
	 */
		PLAYER_LEVEL,
	/*
	 * Inventaire d’un joueur.
	 */
		PLAYER_BAG,
	/*
	 * Un joueur expulse.
	 */
		PLAYER_KICKED,
	/*
	 * Un joueur fait un broadcast.
	 */
		PLAYER_BROADCAST,
	/*
	 * Premier joueur lance l’incantation pour tous les suivants sur la case.
	 */
		PLAYER_LAUNCH_INCANTATION,
	/*
	 * Fin de l’incantation sur la case donnée avec le résultat R (0 ou 1).
	 */
		PLAYER_ENDS_INCANTATION,
	/*
	 * Le joueur pond un oeuf.
	 */
		PLAYER_POOPS_EGG,
	/*
	 * Le joueur jette une ressource.
	 */
		PLAYER_DROP_RESSOURCE,
	/*
	 * Le joueur prend une ressource.
	 */
		PLAYER_GET_RESSOURCE,
	/*
	 * Le joueur est mort de faim.
	 */
		PLAYER_DIED_OF_HUNGER,
	/*
	 * L’œuf a été pondu sur la case par le joueur.
	 */
		PLAYER_POOPED_EGG_ON_CASE,
	/*
	 * L’œuf éclot.
	 */
		EGG_IS_HATCHING,
	/*
	 * Un joueur s’est connecté pour l’œuf.
	 */
		PLAYER_CONNECTED_FOR_EGG,
	/*
	 * L’œuf éclos est mort de faim.
	 */
		EGG_DIED_OF_HUNGER,
	/*
	 * Demande de l’unité de temps courante sur le serveur.
	 */
		ASK_FOR_UNIT_TIME,
	/*
	 * Modification de l’unité de temps sur le serveur.
	 */
		ASK_FOR_UNIT_TIME_MODIFICATION,
	/*
	 * Fin du jeu. L’équipe donnée remporte la partie.
	 */
		GAME_ENDS,
	/*
	 * Message du serveur.
	 */
		MESSAGE_FROM_SERVER,
	/*
	 * Commande inconnue.
	 */
		UNKNOWN_CMD,
	/*
	 * Mauvais paramètres pour la commande.
	 */
		BAD_CMD_PARAMETER
	};
	EventType	identifyEvent(const std::string &cmdMsg);
};

} /* namespace Zappy */
#endif /* SERVERMSGFORMATTER_HH_ */
