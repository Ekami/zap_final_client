/*
 * UICallbacks.hh
 *
 *  Created on: Jun 14, 2013
 *      Author: godard_b
 */

#ifndef IUICALLBACKS_HH_
#define IUICALLBACKS_HH_
#include "Trantorian.hh"
#include "Stone.hh"
#include "Egg.hh"

class IUICallbacks {
public:
	virtual void	initMap() = 0;
	virtual void	clearMap() = 0;
	virtual void	drawTrantorian(const Trantorian &trant) = 0;
	virtual	void	drawEgg(const Egg &egg) = 0;
	virtual void	drawStone(const Stone &stone) = 0;
	virtual	void	moveTrantorian(const Trantorian &trant) = 0;
	virtual 		~IUICallbacks() {}
};

#endif /* UICALLBACKS_HH_ */
