/*
 * Egg.hh
 *
 *  Created on: Jun 15, 2013
 *      Author: godard_b
 */

#ifndef EGG_HH_
#define EGG_HH_

class Egg {
public:
	enum Color
	{
		RED,
		BLUE,
		GREEN,
		BLACK,
		WHITE
	};
	Egg(const int x, const int y, const Color color);
private:
	int		x,y;
	Color	color;
};

#endif /* EGG_HH_ */
